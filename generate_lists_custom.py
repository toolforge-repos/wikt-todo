from datetime import datetime
import sys

# Also import relevant local modules
from output_formatting import OutputFormatter
import wikt_todo_utility

# By default, only generate the first 5000 results for each query (can be overridden
# on a per-todo-list basis)
FALLBACK_ROW_LIMIT = 5000


# Import the todo list definitions from the custom directory
todoListClasses = wikt_todo_utility.get_todo_list_classes_from_directory('custom')

# Get command line arguments
args = wikt_todo_utility.get_command_line_args(queryNames=[todoListObj.todoListName for todoListObj in todoListClasses])

# If a specific todo list was requested, only run that one
if args.name != None:
    todoListClasses = [todoListClass for todoListClass in todoListClasses if todoListClass.todoListName == args.name]
    if len(todoListClasses) == 0:
        print('invalid todo list class - should have been caught by the command line parser!', file=sys.stderr)
        exit(1)



for todoListClass in todoListClasses:
    todoListObj = todoListClass()

    # Is this todo list the one requested by the user?
    if args.name != None and args.name != todoListObj.todoListName:
        continue

    output = OutputFormatter(updateFreq='every Sunday',
                             isUserUpdatable=todoListObj.isUserUpdatable if hasattr(todoListObj, 'isUserUpdatable') else False,
                             rowLimit=todoListObj.rowLimit if hasattr(todoListObj, 'rowLimit') else FALLBACK_ROW_LIMIT)

    for row in todoListObj.run():
        if not output.handle_row(row):
            break

    if args.dry_run:
        print(output.get_wikitext())
    else:
        # Save output to wiki
        output.save_output_to_wiki(todoListObj.todoListName, args.user)
        
        # Also update the last-updated time for this list
        list_last_updated_file = f'last-custom-times/{todoListObj.todoListName}'
        open(list_last_updated_file, 'w').write(str(datetime.utcnow().timestamp()))

    print('%s: updated custom list %s: %s' % (datetime.now().replace(microsecond=0).isoformat(),
                                              todoListObj.todoListName,
                                              '%d results' % output.rowCount))

