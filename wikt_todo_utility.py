"""Various utilities needed by Python todo list definition scripts."""

import argparse
import importlib
import inspect
import json
import os
import pymysql.cursors
import requests
import toolforge



# COMMAND LINE ARGUMENT PARSING

def get_command_line_args(queryNames: list, allowLocalFile: bool=False):
    """
    Sets up an `argparse` command line parser and returns the command line arguments as parsed by it.

    The `allowLocalFile` parameter determines whether arguments for specifying a local dump file
    to work from should be provided.
    """

    parser = argparse.ArgumentParser()
    queryGroup = parser.add_mutually_exclusive_group(required=True)
    queryGroup.add_argument('name', metavar="'todo list name'", choices=list(queryNames), nargs='?', help='name of the single todo list to generate')
    queryGroup.add_argument('--all', action='store_true', help='generate all todo lists')
    parser.add_argument('--dry-run', action='store_true', help="don't save output to the wiki, just print it to stdout (or *.out file in toolforge home directory)")
    parser.add_argument('--user', action='store', help="the username of the user who requested the update, if any")

    if allowLocalFile:
        parser.add_argument('--file', action='store', help='local dump file to read from for testing (if not given, the latest Toolforge NFS dump is used)')

    result = parser.parse_args()

    if allowLocalFile and result.file and not result.dry_run:
        raise Exception('You must supply the --dry-run argument if using --file')

    return result



# GET TODO LIST CLASSES FROM DIRECTORY

def get_todo_list_classes_from_directory(dirname: str) -> list:
    todoListClasses = []

    # Go through every Python file in the given directory and load all the modules
    for file in os.listdir(os.path.dirname(os.path.abspath(__file__)) + '/' + dirname):
        if not file.endswith('.py') or file == '!template.py':
            continue

        module_name = file[:-3]  # Remove the '.py' extension

        module = importlib.import_module(f'{dirname}.{module_name}')
        for name, obj in inspect.getmembers(module):
            if inspect.isclass(obj) and obj not in todoListClasses:
                todoListClasses.append(obj)

    return todoListClasses



# NAMESPACE NUMBER <-> NAME CONVERSION

_nsNumberToName = None

def _load_namespaces():
    """Obtains the latest list of namespaces from the wiki's API."""

    global _nsNumberToName
    
    if not _nsNumberToName:
        params = {
            'action': 'query',
            'meta': 'siteinfo',
            'siprop': 'namespaces',
            'format': 'json',
            'formatversion': '2'
        }

        response = requests.get('https://en.wiktionary.org/w/api.php', params)
        _nsNumberToName = {int(number): data['name'] for number, data in response.json()['query']['namespaces'].items()}

def get_namespace_name(nsNumber: int) -> str | None:
    _load_namespaces()

    global _nsNumberToName
    if nsNumber in _nsNumberToName:
        return _nsNumberToName[nsNumber]
    return None

def get_namespace_prefix(nsNumber: int) -> str | None:
    if nsNumber == 0:
        return ''
    return f'{get_namespace_name(nsNumber)}:'



# LANGUAGE CODE <-> NAME CONVERSION

_lolLoaded = False
_languageNameToCode = dict()
_languageCodeToName = dict()

def _load_list_of_languages():
    """Obtains the latest version of WT:LOLCSV from the wiki."""

    global _lolLoaded, _languageCodeToName, _languageNameToCode
    
    if not _lolLoaded:
        params = {
            'action': 'expandtemplates',
            'text': '{{#invoke:list of languages, csv format|show}}',
            'prop': 'wikitext',
            'format': 'json',
        }

        response = requests.get('https://en.wiktionary.org/w/api.php', params)
        data = response.json()['expandtemplates']['wikitext'].replace('<pre>\n', '').replace('</pre>', '')
        for line in data.split('\n'):
            pieces = line.split(';', 4)
            _languageNameToCode[pieces[2]] = pieces[1]
            _languageCodeToName[pieces[1]] = pieces[2]
        _lolLoaded = True

def get_language_name(languageCode: str) -> str | None:
    _load_list_of_languages()

    global _languageCodeToName
    if languageCode in _languageCodeToName:
        return _languageCodeToName[languageCode]
    return None

def get_language_code(languageName: str) -> str | None:
    _load_list_of_languages()

    global _languageNameToCode
    if languageName in _languageNameToCode:
        return _languageNameToCode[languageName]
    return None



# LANGUAGE/VARIETY/FAMILY HIERARCHY

_hierLoaded = False
_hierarchy = dict()   # key: 'language code', value: ['immediate ancestor family code', 'next ancestor family code', ...]
_varietyCodeToParentCode = dict()  # key: 'variety code', value: 'primary language code or parent variety code'

def _load_language_hierarchy():
    """Obtains the latest version of the language hierarchy from the relevant Lua data modules on the wiki."""

    global _hierLoaded, _hierarchy, _varietyCodeToParentCode
    
    if not _hierLoaded:
        params = {
            'action': 'expandtemplates',
            'text': '{' + 
                '"language":{{#invoke:JSON data|export_languages||ancestors|3}},' +
                '"etymlang":{{#invoke:JSON data|export_etymology_languages}},'
                '"family":{{#invoke:JSON data|export_families}}' +
            '}',
            'prop': 'wikitext',
            'format': 'json',
        }

        response = requests.get('https://en.wiktionary.org/w/api.php', params)
        dataRaw = response.json()['expandtemplates']['wikitext'].replace('<pre>\n', '').replace('</pre>', '')
        data = json.loads(dataRaw)

        # Get the list of families for every language
        for lang in sorted(data['language'].keys()):
            _hierarchy[lang] = []

            if '3' in data['language'][lang]:
                # Construct this language's family tree
                family = data['language'][lang]['3']

                while family not in _hierarchy[lang]:
                    _hierarchy[lang].append(family)

                    # What family does this family belong to?
                    if family in data['family'] and '3' in data['family'][family]:
                        family = data['family'][family]['3']
                    else:
                        break

        # Populate a map from etymology-only language codes (variety codes) to the parent code
        # (either the primary language code or a parent variety code)
        for lang in sorted(data['etymlang'].keys()):
            if '5' in data['etymlang'][lang]:
                _varietyCodeToParentCode[lang] = data['etymlang'][lang]['5']

        _hierLoaded = True

def get_family_tree_of_language(languageCode: str) -> tuple[str, ...] | None:
    _load_language_hierarchy()

    global _hierarchy
    if languageCode in _hierarchy:
        return _hierarchy[languageCode]
    return None

def get_primary_code(maybeVarietyCode: str) -> str | None:
    _load_language_hierarchy()

    global _varietyCodeToParentCode

    codeToCheck = maybeVarietyCode
    # recursion protection: maximum 20 steps
    for i in range(20):
        if codeToCheck not in _varietyCodeToParentCode:
            break
        codeToCheck = _varietyCodeToParentCode[codeToCheck]
        
    return codeToCheck



# FORM-OF TEMPLATES

def get_form_of_templates() -> list:
    """
    Obtains a list of names of all form-of templates, including redirects.
    """

    if 'data' not in get_form_of_templates.__dict__:
        with toolforge.connect("enwiktionary") as conn:
            cur: pymysql.cursors.Cursor
            with conn.cursor(pymysql.cursors.Cursor) as cur:
                query = \
"""
WITH form_of_templates AS (
  SELECT page_title
  FROM page
    INNER JOIN categorylinks ON cl_from = page_id
  WHERE page_namespace = 10
    AND (cl_to = 'Form-of_templates' OR cl_to LIKE '%_form-of_templates')
)
SELECT page_title
FROM form_of_templates
UNION
SELECT page_title
FROM page
  INNER JOIN redirect ON rd_from = page_id
WHERE page_namespace = 10
  AND rd_namespace = 10
  AND rd_title IN (SELECT page_title
                   FROM form_of_templates)
"""
                cur.execute(query)
                get_form_of_templates.data = set([row[0].decode('utf-8').replace('_', ' ') for row in cur.fetchall()])

    return get_form_of_templates.data



# GET PAGE SET FROM WIKI

def get_page_set_from_wiki(pageNs: int, pageTitle: str) -> set:
    """
    Obtains a list of all links on a certain page, including redirects.
    """
    
    with toolforge.connect("enwiktionary") as conn:
        cur: pymysql.cursors.Cursor
        with conn.cursor(pymysql.cursors.Cursor) as cur:
            query = \
"""
WITH page_set_raw AS (
  SELECT lt_namespace, lt_title
  FROM page
    INNER JOIN pagelinks ON page_id = pl_from
    INNER JOIN linktarget ON pl_target_id = lt_id
  WHERE page_namespace = %s
    AND page_title = %s
),
page_set_with_redirect_targets AS (
  SELECT lt_namespace, lt_title
  FROM page_set_raw
  UNION
  -- also select the targets of any redirects in the set
  SELECT rd_namespace, rd_title
  FROM page
    INNER JOIN redirect ON rd_from = page_id
    INNER JOIN page_set_raw ON (page_namespace = lt_namespace AND page_title = lt_title)
)
-- select the pages themselves
SELECT lt_title
FROM page_set_with_redirect_targets
UNION
-- also select any redirects to pages in the set
SELECT page_title
FROM page
  INNER JOIN redirect ON rd_from = page_id
  INNER JOIN page_set_with_redirect_targets ON (rd_namespace = lt_namespace AND rd_title = lt_title)
"""
            cur.execute(query, (pageNs, pageTitle.replace(' ', '_')))
            return set([row[0].decode('utf-8').replace('_', ' ') for row in cur.fetchall()])
