import sys
import toolforge
import pymysql.cursors
from datetime import datetime
import traceback

# Import the SQL query definitions from the sql/queries.py file
from sql.queries import queries

# Also import relevant local modules
from output_formatting import OutputFormatter
import wikt_todo_utility

# Only generate the first 5000 results for each query
ROW_LIMIT = 5000


# Get command line arguments
args = wikt_todo_utility.get_command_line_args(queryNames=queries.keys())

# Keep track of whether we experienced any failures along the way
failure = False

# Iterate through the queries, running each one on a separate DB connection
for queryName, sql in queries.items():
    # Is this query the one requested by the user?
    if args.name != None and args.name != queryName:
        continue

    output = OutputFormatter(updateFreq='every Sunday', isUserUpdatable=True, rowLimit=ROW_LIMIT)

    # Open a connection to Wiktionary's database, using the 'analytics' cluster for
    # longer timeouts (apparently 3 hours instead of 5 minutes for the default 'web')
    with toolforge.connect("enwiktionary", cluster='analytics', read_timeout=999999, write_timeout=999999) as conn:  # conn is a pymysql.connection object

        # Execute the query and iterate over results
        cur: pymysql.cursors.SSDictCursor
        with conn.cursor(pymysql.cursors.SSDictCursor) as cur:
            try:
                cur.execute("%s LIMIT %d" % (sql, ROW_LIMIT + 1))

                # Iterate row by row through the results
                while True:
                    row = cur.fetchone()

                    # If we have reached the end of the data or the row limit, stop
                    if not row or not output.handle_row(row):
                        break

            except pymysql.err.Error as e:
                print(queryName, 'FAILED: ', ''.join(traceback.format_exception(e)), file=sys.stderr)
                failure = True
                continue

    if args.dry_run:
        print(output.get_wikitext())
    else:
        # Save output to wiki
        output.save_output_to_wiki(queryName, args.user)
        
        # Also update the last-updated time for this query
        list_last_updated_file = f'last-sql-times/{queryName}'
        open(list_last_updated_file, 'w').write(str(datetime.utcnow().timestamp()))

    print('%s: updated query %s: %s' % (datetime.now().replace(microsecond=0).isoformat(),
                                        queryName,
                                        '%d results' % output.rowCount))

# If there was a failure, exit with a nonzero exit code to trigger error emails
if failure:
    exit(1)
