import mwparserfromhell
import re

class ManuallyCraftedLabels:
    todoListName = 'Manually crafted labels'

    def __init__(self):
        self._results = []

    def handle_line(self, pageId: int, pageNs: int, pageTitle: str, currentSections: dict[int, str | None], 
                    line: str, lineParsed: mwparserfromhell.wikicode.Wikicode | None) -> None:
        raise NotImplementedError

        if pageNs not in (0, 114, 118):
            return
        
        if re.search(r'^#+\s*(\{\{(sense|a|q)[|}]|\'\'\(|\(\'\')', line):
            self._results.append({
                'SECTIONHEADING': currentSections[2],
                'Page_NSTITLE_EDITLINK': f'{pageNs}|{pageTitle}',
                'Offending wikitext_CODE50': line
            })

    def get_results(self) -> list[dict[str, str | int | None]]:
        return sorted(self._results, key=lambda row: (row['SECTIONHEADING'], row['Page_NSTITLE_EDITLINK']))
