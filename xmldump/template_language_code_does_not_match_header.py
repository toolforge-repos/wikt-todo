import mwparserfromhell
import wikt_todo_utility
import re

class TemplateLanguageCodeDoesNotMatchHeader:
    todoListName = 'Template language code does not match header'

    def __init__(self):
        self._results = []

        # For local debugging only - list as of 27 July 2024
        # self.templatesToIgnore = set([
        #     '...', '…', '1', 'abbr', 'ady-decl-noun', 'ady-decl-noun2', 'ady-decl-noun3', 'ady-decl-noun4', 
        #     'ady-noun-decl', 'also', 'alt-de-ch', 'alt-parts', 'anchor', 'Anchor', 'angbr', 'angbr IPA', 
        #     'Angle bracket', 'antsense', 'apdx-t', 'ar-root', 'as', 'att', 'attention', 'attn', 
        #     'bnt-desc', 'cap', 'chars', 'chu Han form of-lite', 'cite book', 'Cite book', 'cite-av', 
        #     'cite-book', 'cite-journal', 'cite-text', 'cite-web', 'cmn-ear-l', 'cog', 'cognate', 
        #     'color', 'color panel', 'colour panel', 'com-ja', 'datedef', 'def-date', 'defdate', 
        #     'desc', 'desc/sl-tonal', 'descendant', 'descendants tree', 'desctree', 'displaced', 
        #     'egy-glyph', 'egy-glyph-img', 'el-UK-US', 'enPR', 'enPRchar', 'epinew', 'ethnologue', 
        #     'ett-script', 'fa-regional', 'fa-rtl', 'false cognate', 'false cognates', 'g', 
        #     'gl', 'glink/ja', 'gloss', 'glossary', 'grc-ark', 'grc-att', 'grc-dor', 'grc-epi', 
        #     'grc-ion', 'han tu form of-lite', 'he-root', 'Hira-script', 'i', 'inc-ext', 'inc-extension', 
        #     'interwiktionary', 'Interwiktionary', 'IPAfont', 'ISO 639', 'italic', 'ja-com', 'ja-compound', 
        #     'ja-glossary', 'ja-r', 'ja-r/syn', 'ja-rendaku2', 'Kana-script', 'kmas-noun-c', 'ko-inline', 
        #     'ko-l', 'ko-ref', 'l', 'L', 'l-de-nom', 'l-nb', 'l-nn', 'l-self', 'l-UK-US', 'lang', 
        #     'langcat', 'langindex', 'langlist', 'langname', 'langname-mention', 'languageindex', 
        #     'lect', 'letter disp2', 'lg', 'linguist list', 'Linguist List', 'link', 'lit', 'liushu', 
        #     'll', 'lw', 'm', 'm-g', 'm-self', 'm+', 'mention', 'mention-gloss', 'merge', 'mg', 
        #     'mono', 'monospace', 'mul-script', 'mul-script/Hira', 'mul-script/Kana', 'mul-script/Latn/groups', 
        #     'n-g', 'nat-res', 'native or resident of', 'nc', 'ncog', 'ng', 'ngd', 'nl', 'no attested translation', 
        #     'no direct idiomatic translation', 'no equivalent translation', 'nobold', 'nobr', 'noitalic', 
        #     'non-gloss', 'non-gloss definition', 'non-lemma', 'noncog', 'noncognate', 'nonlemma', 
        #     'nonlemmas', 'not used', 'nowrap', 'okm-inline', 'okm-l', 'orth', 'ortho', 'orthography', 
        #     'overline', 'p', 'para', 'pcp-cmp', 'pedia', 'pedialite', 'picdic/label', 'picdicimg', 
        #     'picdiclabel', 'picdiclabel/new', 'pre-Germanic', 'q', 'q-g', 'qf', 'qinfl', 'qual', 
        #     'qualifier', 'qualifier-inflection', 'quote-gloss', 'quoted term', 'rendaku2', 'rfd', 
        #     'rfdo', 'rfv', 'ryu-r', 's', 'sa-af', 'sa-com', 'sa-root', 'see also', 'see-temp', 
        #     'sense', 'SI-unit-abb', 'SI-unit-abb2', 'SI-unit-abbnp', 'sic', 'SIC', 'sic.', 'sit-loan', 
        #     'small', 'small caps', 'smallcaps', 'smc', 'sp', 'spaces', 'Spaces', 'sub', 'SUB', 
        #     'sup', 'SUP', 'superscript', 'syc-root', 'syc-root-entry', 'syn-saurus', 't', 't-', 
        #     't-check', 't-egy', 't-image', 't-needed', 't+', 't+check', 't2i-Egyd', 'trans-see', 
        #     'trans-top', 'trans-top-also', 'trans-top-see', 'transterm', 'tt', 'tt-check', 'tt+', 
        #     'tt+check', 'ttbc', 'vern', 'vernacular', 'waei', 'wasei eigo', 'Webster 1913', 'wikipedia', 
        #     'wp', 'xlit'
        # ])
        # self.templatesToIgnoreInMulSection = set([
        #     'Q', 'IPA', 'IPA letters', 'IPA+', 'quote-journal', 'uxi', 'audio', 'ux', 'usex',
        #     'quote-book', 'quote-newsgroup', 'quote-web', 'quote', 'quote-text', 'quote-video game',
        #     'ja-forms'
        # ])
        # return

        # Templates for which the first parameter DOES NOT have to match the section's own language code
        self.templatesToIgnore = \
            wikt_todo_utility.get_page_set_from_wiki(4, 'Todo/Lists/Template language code does not match header/Excluded templates')
        # Additional templates to skip in Translingual sections
        self.templatesToIgnoreInMulSection = \
            wikt_todo_utility.get_page_set_from_wiki(4, 'Todo/Lists/Template language code does not match header/Further exclusions in Translingual sections')


    def handle_line(self, pageId: int, pageNs: int, pageTitle: str, currentSections: dict[int, str | None], 
                    line: str, lineParsed: mwparserfromhell.wikicode.Wikicode | None) -> None:
        if pageNs not in [0, 118]:
            return
        
        # We're only interested in lines of wikitext that contain templates
        if lineParsed == None:
            return
        
        # TODO special case
        if pageTitle == 'Taumatawhakatangihangakoauauotamateaturipukakapikimaungahoronukupokaiwhenuakitanatahu':
            return
        
        if currentSections[2] == None:
            return
        
        languageCode = wikt_todo_utility.get_language_code(currentSections[2])
        if languageCode == None:
            # don't know this language
            return
        
        # Look for templates in this line of wikitext
        template: mwparserfromhell.wikicode.Template
        for template in lineParsed.filter_templates():
            templateName = str(template.name).replace('_', ' ').strip()

            # There are two types of templates we are concerned with.
            # Style 1 has the language code as the first unnamed parameter:
            #     {{bor|en|...}}
            # Style 2 has the language code prefixed to the template name:
            #     {{fr-IPA|...}}
            templateStyle = 1

            # Check to see if we have a template of style 2
            langcodePrefixCandidateShort = None
            langcodePrefixCandidateLong = None
            if len(templateName) >= 4 and templateName[2] == '-':
                langcodePrefixCandidateShort = templateName[0:2]  # Example: fr
            elif len(templateName) >= 5 and templateName[3] == '-':
                langcodePrefixCandidateShort = templateName[0:3]  # Example: enm
                if len(templateName) >= 9 and templateName[7] == '-':
                    langcodePrefixCandidateLong = templateName[0:7]  # Example: roa-opt
                elif len(templateName) >= 8 and templateName[6] == '-':
                    langcodePrefixCandidateLong = templateName[0:6]  # Example: nds-de
            
            # Are these potential prefixes actually genuine language codes or variety codes?
            # (also include a special condition for proto-languages, whose templates are
            # generally prefixed with a version of the language code lacking the -pro suffix)
            if langcodePrefixCandidateLong != None and wikt_todo_utility.get_language_name(langcodePrefixCandidateLong) != None:
                templateStyle = 2
                templateLangCode = langcodePrefixCandidateLong
            elif langcodePrefixCandidateLong != None and wikt_todo_utility.get_language_name(langcodePrefixCandidateLong + '-pro') != None:
                templateStyle = 2
                templateLangCode = langcodePrefixCandidateLong + '-pro'
            elif langcodePrefixCandidateShort != None and wikt_todo_utility.get_language_name(langcodePrefixCandidateShort) != None:
                templateStyle = 2
                templateLangCode = langcodePrefixCandidateShort
            elif langcodePrefixCandidateShort != None and wikt_todo_utility.get_language_name(langcodePrefixCandidateShort + '-pro') != None:
                templateStyle = 2
                templateLangCode = langcodePrefixCandidateShort + '-pro'
            
            # Look for templates that should be skipped in template style 2 only
            if templateStyle == 2:
                # Ignore collapsible box templates and link templates ending in certain common patterns
                if templateName in (templateLangCode + '-l',
                                    templateLangCode + '-m',
                                    templateLangCode + '-top',
                                    templateLangCode + '-top3',
                                    templateLangCode + '-top4',
                                    templateLangCode + '-top5',
                                    templateLangCode + '-bottom'):
                    continue

                # Ignore language-specific IPA templates in Translingual sections
                if languageCode == 'mul' and templateName == templateLangCode + '-IPA':
                    continue

                # Templates to always treat as template style 1, even though they
                # look like template style 2
                # Example: {{col-auto|en|...}} where "col" is the language code for Columbia-Wenatchi
                if templateName in (templateLangCode + '-auto',
                                    templateLangCode + '-lite',
                                    'def-uncertain',
                                    'def-unc',
                                    'def-see'):
                    templateStyle = 1

            if templateStyle == 1:
                # Make sure it has an unnamed parameter
                if not template.has(1):
                    continue

                templateLangCode = str(template.get(1).value).strip()

                # Make sure the parameter looks like a language code
                if not re.match(r'^[a-z]{2,3}(?:-[a-z]{2,3}){0,2}$', templateLangCode):
                    continue

                # Map variety codes to the primary language code
                templateLangCode = wikt_todo_utility.get_primary_code(templateLangCode)

            # If the template's language code matches the section's language code,
            # it is fine - skip it
            if templateLangCode == languageCode:
                continue

            # Treat Sinitic (zhx), Arabic family (sem-arb), and Norwegian varieties as equivalent
            familiesOfLanguageCode = wikt_todo_utility.get_family_tree_of_language(languageCode)
            familiesOfTemplateLangCode = wikt_todo_utility.get_family_tree_of_language(templateLangCode)
            if (
                familiesOfLanguageCode != None and
                familiesOfTemplateLangCode != None and
                (
                    ('zhx' in familiesOfLanguageCode and 'zhx' in familiesOfTemplateLangCode) or
                    ('sem-arb' in familiesOfLanguageCode and 'sem-arb' in familiesOfTemplateLangCode)
                )
            ):
                continue
            if languageCode in ('nb', 'nn', 'no') and templateLangCode in ('nb', 'nn', 'no'):
                continue

            # If the style 2 template's language code matches the section's language code without
            # the -pro suffix, skip it
            if templateStyle == 2 and templateLangCode == languageCode.replace("-pro", ""):
                continue

            # If the template is on the ignore list, or is a reference, quote,
            # or usage note template, skip it
            if (templateName in self.templatesToIgnore or
                templateName.replace('-lite', '') in self.templatesToIgnore or
                re.match(f'(R:|RQ:|U:)', templateName)):
                continue

            # If in a Translingual section, skip templates on the Translingual-section ignore list
            if languageCode == 'mul' and (templateName in self.templatesToIgnoreInMulSection or
                                          templateName.replace('-lite', '') in self.templatesToIgnoreInMulSection):
                continue

            # Skip etymology templates with "nocat=" specified
            if template.has('nocat', ignore_empty=True):
                continue

            # Skip quote templates where the term language is indicated
            if template.has('termlang') and str(template.get('termlang').value) == languageCode:
                continue

            # Low German codes are an exceptional case; templates prefixed with nds-
            # are used in both nds-de and nds-nl entries (but there are some nds-de-
            # prefixed templates that are only for use in nds-de entries)
            if languageCode in ('nds-de', 'nds-nl') and templateLangCode == 'nds':
                continue

            # TODO temp SPECIAL CASE to skip {{alter}} and {{syn}} templates with a Malay<->Indonesian pairing
            if languageCode in ['id', 'ms'] and templateLangCode in ['id', 'ms'] and templateName in ['alter', 'syn']:
                continue

            # SPECIAL CASE to skip style 2 templates in various intra-family pairings
            # TODO avoid hard-coding these - base it on family definitions in the Wikt language tree
            intraFamilyPairings = [
                ('ja', 'ojp', 'jpx', 'jpx-pro', 'jpx-ryu', 'jpx-ryu-pro'),  # Japanese
                ('ko', 'ko-ear', 'okm', 'oko'),                             # Korean
                ('ku', 'ku-pro', 'ckb', 'sdh', 'kmr'),                      # Kurdish
                ('pa', 'pnb'),                                              # Punjabi
            ]
            if templateStyle == 2:
                shouldContinue = False
                for lectSet in intraFamilyPairings:
                    if languageCode in lectSet and templateLangCode in lectSet:
                        # the l2 lang and template lang are in the same lect set - skip this template and continue
                        shouldContinue = True
                        break
                
                if shouldContinue:
                    continue

            # Mismatch!
            templateLangName = wikt_todo_utility.get_language_name(templateLangCode)
            self._results.append({
                'SECTIONHEADING': '<nowiki>{{%s}}</nowiki>' % templateName,
                'Template_NOWIKI': templateName,
                'Page_PAGE_EDITLINK': f'{pageTitle}#{currentSections[2]}',
                'Language header_RAW': f'{currentSections[2]} ({languageCode})',
                'Code_RAW': f'{templateLangCode} ({templateLangName if templateLangName else "???"})',
                'Offending wikitext line (first 100 chars)_CODE100': line
            })
        
        # For debugging only
        # if pageId > 10000:
        #     for result in self.get_results():
        #         print(result)
        #     exit()


    def get_results(self) -> list:
        return sorted(self._results, key=lambda row: (row['SECTIONHEADING'][10:-11], row['Template_NOWIKI'], row['Page_PAGE_EDITLINK']))
