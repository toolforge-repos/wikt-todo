import mwparserfromhell

class BrokenLinksToSenseidEtymid:
    todoListName = 'Broken links to senseid and etymid'

    def __init__(self):
        # {{senseid}}/{{etymid}} templates that have been found
        # set of tuples: (title, template language, senseid/etymid value)
        self.foundIds = set()

        # {{l|...|id=}} values that have not (yet) been reconciled with a senseid/etymid
        # dictionary: (title of target page, template language, id= parameter) -> [title of source page, title of source page, ...]
        self.unaccountedIds = dict()


    def handle_line(self, pageId: int, pageNs: int, pageTitle: str, currentSections: dict[int, str | None], 
                    line: str, lineParsed: mwparserfromhell.wikicode.Wikicode | None) -> None:
        # Only handle main-namespace entries in English for now
        if pageNs != 0 or currentSections[2] != 'English':
            return

        # We're only interested in templates
        if lineParsed == None:
            return
        
        emptyParam = mwparserfromhell.nodes.extras.parameter.Parameter
        
        # Look for form-of templates in this line of wikitext
        template: mwparserfromhell.wikicode.Template
        for template in lineParsed.filter_templates():
            templateName = str(template.name).replace('_', ' ')

            if templateName in ['senseid', 'sid', 'etymid']:
                tuple = (pageTitle, str(template.get(1, emptyParam).value), str(template.get(2, emptyParam).value).replace('_', ' '))

                # Only English for now
                if tuple[1] != 'en':
                    continue

                # Record this senseid/etymid in the set
                self.foundIds.add(tuple)

                # If this senseid/etymid is listed in unaccountedIds, remove it
                if tuple in self.unaccountedIds:
                    self.unaccountedIds.pop(tuple)
            
            elif template.has_param('id', True) or template.has_param('id1', True) or template.has_param('id2', True) or \
                    template.has_param('id3', True) or template.has_param('id4', True):
                # Skip templates that use the id= parameter for something else
                if templateName.startswith('quote-') or templateName == 'rootsee' or templateName == 'etymon':
                    continue

                for param in ['id', 'id1', 'id2', 'id3', 'id4']:
                    if template.has_param(param, True):
                        id = str(template.get(param, emptyParam).value)
                        break

                # Which page name does the senseid template correspond to?
                targetPage = str(template.get(2, emptyParam).value)
                if param == 'id':
                    if templateName in ['inherited', 'inh', 'inh+',
                                        'derived', 'der', 'der+',
                                        'borrowed', 'bor', 'bor+',
                                        'root']:
                        targetPage = str(template.get(3, emptyParam).value)
                        targetLang = str(template.get(2, emptyParam).value)
                    elif templateName in ['suffixsee', 'prefixsee'] and not template.has_param('head'):
                        # For these templates, the target page is the page they are on.
                        # We skip transclusions with the head= parameter because this 
                        # is often used as a hack, e.g. at [[-o]]:
                        #   {{suffixsee|it|head=-o (deverbal)}}
                        targetPage = pageTitle
                        targetLang = str(template.get(1, emptyParam).value)
                    else:
                        targetPage = str(template.get(2, emptyParam).value)
                        targetLang = str(template.get(1, emptyParam).value)
                elif templateName == 'root':
                    targetPage = str(template.get(int(param[-1]) + 2, emptyParam).value)
                    targetLang = str(template.get(2, emptyParam).value)
                else:
                    targetPage = str(template.get(int(param[-1]) + 1, emptyParam).value)
                    targetLang = str(template.get(1, emptyParam).value)  # unless this has been overridden using some magic trickery :/

                # Only English for now
                if targetLang != 'en':
                    continue

                # Special handling for {{prefix}} and {{suffix}}
                if templateName in ['prefix', 'pre', 'confix', 'con'] and param == 'id1' and not targetPage.endswith('-'):
                    targetPage = targetPage + '-'
                elif (
                    (templateName in ['suffix', 'suf'] and param == 'id2') or
                    (templateName in ['confix', 'con'] and param == ('id3' if template.has_param(4, True) else 'id2')) 
                ) and not targetPage.startswith('-'):
                    targetPage = '-' + targetPage
                
                tuple = (targetPage.replace('_', ' '), targetLang, id.replace('_', ' '))

                # is this senseid/etymid unaccounted for?
                if tuple not in self.foundIds:
                    if tuple not in self.unaccountedIds:
                        self.unaccountedIds[tuple] = []
                    self.unaccountedIds[tuple].append(pageTitle) 


    def get_results(self) -> list[dict[str, str | int | None]]:
        results = []
        for tuple, pages in self.unaccountedIds.items():
            for page in pages:
                results.append({
                    'SECTIONHEADING': tuple[0][0].upper() if (len(tuple[0]) >= 1 and tuple[0][0].upper() >= 'A' and tuple[0][0].upper() <= 'Z') else 'other',
                    'Page_PAGE_EDITLINK': page,
                    'Linked page_PAGE_EDITLINK': tuple[0],
                    'Linked senseid/etymid_RAW': tuple[2]
                })

        return sorted(results, key=lambda row: (row['SECTIONHEADING'], row['Linked page_PAGE_EDITLINK'], row['Linked senseid/etymid_RAW'], row['Page_PAGE_EDITLINK']))
