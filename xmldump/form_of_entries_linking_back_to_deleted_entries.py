import pymysql
import pymysql.cursors
import mwparserfromhell
import toolforge

# Local import
import wikt_todo_utility

class FormOfEntriesLinkingBackToDeletedEntries:
    todoListName = 'Form-of entries linking back to deleted entries'

    def __init__(self):
        self._results = []

        # Get a list of all entries that once existed (log entries are present) but now do not exist
        # As of January 2024 there were around 340,000 titles on this list
        with toolforge.connect("enwiktionary") as conn:
            cur: pymysql.cursors.Cursor
            with conn.cursor(pymysql.cursors.Cursor) as cur:
                query = \
"""
SELECT DISTINCT log_title
FROM logging
  LEFT JOIN page ON page_namespace = 0 AND page_title = log_title
WHERE log_namespace = 0 
  AND page_id IS NULL
"""
                cur.execute(query)
                self._deletedEntries = set([row[0].decode('utf-8').replace('_', ' ') for row in cur.fetchall()])


    def handle_line(self, pageId: int, pageNs: int, pageTitle: str, currentSections: dict[int, str | None], 
                    line: str, lineParsed: mwparserfromhell.wikicode.Wikicode | None) -> None:
        # Only handle main-namespace entries in English for now
        if pageNs != 0 or currentSections[2] != 'English':
            return
        
        emptyParam = mwparserfromhell.nodes.extras.parameter.Parameter

        # Look only at sense lines that contain templates
        if not line.startswith('#') or lineParsed == None:
            return
        
        # Look for form-of templates in this line of wikitext
        template: mwparserfromhell.wikicode.Template
        for template in lineParsed.filter_templates():
            templateName = str(template.name).replace('_', ' ')
            if templateName in wikt_todo_utility.get_form_of_templates():
                # Need to handle the "form of" template specially, as it has an extra parameter
                if templateName == 'form of':
                    templateName += ' [%s]' % str(template.get(2, emptyParam).value)
                    linkedPage = str(template.get(3, emptyParam).value)
                # Is this a language-specific form-of template? These do not have a language code param
                elif templateName[2] == '-' or templateName[3] == '-':
                    linkedPage = str(template.get(1, emptyParam).value)
                else:
                    linkedPage = str(template.get(2, emptyParam).value)

                # Remove underscores and strip any anchor
                linkedPage = linkedPage.replace('_', ' ').split('#', 1)[0]

                # Has the linked entry been deleted?
                if linkedPage != '' and linkedPage in self._deletedEntries:
                    self._results.append({
                        'SECTIONHEADING': currentSections[2], # current L2 header
                        'Form-of entry_PAGE_EDITLINK_HISTLINK': pageTitle,
                        'Form-of template_NOWIKI': templateName,
                        'Links to_PAGE_TALKLINK': linkedPage
                    })


    def get_results(self) -> list[dict[str, str | int | None]]:
        return sorted(self._results, key=lambda row: (row['SECTIONHEADING']))
