import mwparserfromhell

class MyTodoList:
    """
    Give the todo list a succinct name. This is used as the name of the WT:Todo/Lists/... subpage.
    """
    todoListName = 'My todo list'

    def __init__(self):
        # This is an example - we set up somewhere to store results
        self._results = []

    def handle_line(self, pageId: int, pageNs: int, pageTitle: str, currentSections: dict[int, str | None], 
                    line: str, lineParsed: mwparserfromhell.wikicode.Wikicode | None) -> None:
        """
        The handle_line function is called for every line of wikitext in every page of the wiki.

        The function is supplied with the following arguments:
        - pageId           the page ID (page_id column in the page table of the database)
        - pageNs           the page's namespace number. See [[WT:Namespaces]].
        - pageTitle        the page title (using spaces, not underscores)
        - currentSections  a dictionary of current section headings. See below.
        - line             the current line of wikitext
        - lineParsed       if the current line of wikitext contains '{{', this parameter
                           contains the parsed wikitext object corresponding to the current
                           line. Otherwise it is set to None for performance reasons.

        The function is called for pages in every namespace, so you will almost certainly want to
        start your code by checking pageNs.
                    
        The dictionary of current section headings contains keys from 1 through 6 corresponding to
        the section hierarchy applicable to the current line. The language name applicable to the
        current line, if any, can be found at currentSections[2]. (Malformed entries and lines with
        the {{also}} template at the top of entries will have currentSections[2] == None.)
        """
        pass

    def get_results(self) -> list[dict[str, str | int | None]]:
        """
        This function is called when the XML dump has finished parsing and the output formatter is
        ready to retrive the final results of the todo list.

        This function is useful for performing post-processing, such as sorting and
        removing duplicates.

        EVERY dictionary in the returned list must have THE SAME keys. Otherwise there will be errors.
        """
        return self._results
