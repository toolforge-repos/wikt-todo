import mwparserfromhell
import pywikibot
import wikt_todo_utility
import re
    

# Checks a result tuple to see if the offending L2 header text
# is still present in the wikitext. Returns true if it is,
# false if it is not 
def bad_l2_still_present(t: tuple[str, str]):
    site = pywikibot.Site()
    page = pywikibot.Page(site, t[0])
    return re.search(r'^==\s*' + re.escape(t[1]), page.text, re.MULTILINE) != None


class LanguageHeadersNotInWTLOL:
    todoListName = 'Language headers not in WT:LOL'

    def __init__(self):
        self._results = set()

    def handle_line(self, pageId: int, pageNs: int, pageTitle: str, currentSections: dict[int, str | None], 
                    line: str, lineParsed: mwparserfromhell.wikicode.Wikicode | None) -> None:
        
        if pageNs not in (0, 118):
            return
        
        if currentSections[2] != None and wikt_todo_utility.get_language_code(currentSections[2]) == None:
            self._results.add((pageTitle, currentSections[2]))

    def get_results(self) -> list[dict[str, str | int | None]]:
        # If a bot is checking for these, it could get through them
        # quite quickly after the dump is released. So we need to check
        # the pages to see if they have already been cleaned. Also 
        # convert the tuples to dictionaries and sort output
        return sorted(({
                'SECTIONHEADING': t[1][0] if (len(t[1]) >= 1 and t[1][0] >= 'A' and t[1][0] <= 'Z') else 'other',
                'Page_PAGE_EDITLINK': t[0],
                'Language_RAW': t[1]
            } for t in self._results if bad_l2_still_present(t)), key=lambda row: (row['SECTIONHEADING'], row['Language_RAW'], row['Page_PAGE_EDITLINK']))
