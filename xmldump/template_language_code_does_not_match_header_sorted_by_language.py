from .template_language_code_does_not_match_header import TemplateLanguageCodeDoesNotMatchHeader

class TemplateLanguageCodeDoesNotMatchHeaderSortedByLanguage(TemplateLanguageCodeDoesNotMatchHeader):
    todoListName = 'Template language code does not match header (sorted by language)'

    def get_results(self) -> list:
        languageCount = dict()

        # Change section heading to L2 language, and identify the most commonly present languages in the list
        for result in self._results:
            result['SECTIONHEADING'] = result['Language header_RAW']
            if result['Language header_RAW'] not in languageCount:
                languageCount[result['Language header_RAW']] = 0
            languageCount[result['Language header_RAW']] += 1

        return sorted(self._results, key=lambda row: (-languageCount[row['Language header_RAW']], row['SECTIONHEADING'], row['Template_NOWIKI'], row['Page_PAGE_EDITLINK']))
