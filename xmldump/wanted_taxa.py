import mwparserfromhell

class WantedTaxa:
    todoListName = 'Wanted taxa'

    def __init__(self):
        # Translingual taxon entries that have been found, and the taxonomic rank at which
        # they lie, according to the {{taxon}} template inside the entry
        # set of tuples: (entry name, taxonomic rank)
        self.foundTaxa = set()

        # {{tax(link|link2|fmt)}} that have not (yet) been reconciled with a taxon entry
        # dictionary: (entry name, taxonomic rank) -> [(title of source page, L2 header), (title of source page, L2 header), ...]
        self.unaccountedTaxonLinks = dict()


    def handle_line(self, pageId: int, pageNs: int, pageTitle: str, currentSections: dict[int, str | None], 
                    line: str, lineParsed: mwparserfromhell.wikicode.Wikicode | None) -> None:
        # Only handle main-namespace entriesfor now
        if pageNs != 0:
            return

        # We're only interested in lines containing templates
        if lineParsed == None:
            return
        
        emptyParam = mwparserfromhell.nodes.extras.parameter.Parameter
        
        # Look for form-of templates in this line of wikitext
        template: mwparserfromhell.wikicode.Template
        for template in lineParsed.filter_templates():
            templateName = str(template.name).replace('_', ' ')

            if templateName == 'taxon':
                taxonTuple = (pageTitle, str(template.get(1, emptyParam).value))

                # Record this taxon in the set
                self.foundTaxa.add(taxonTuple)

                # If this taxon is listed in unaccountedTaxonLinks, remove it
                if taxonTuple in self.unaccountedTaxonLinks:
                    self.unaccountedTaxonLinks.pop(taxonTuple)
            
            elif templateName in ['taxlink', 'taxlink2', 'taxfmt']:
                # param 1 is the linked page, param 2 is the taxonomic rank
                taxonTuple = (str(template.get(1, emptyParam).value).replace('_', ' '), str(template.get(2, emptyParam).value))
            
                # is this taxon unaccounted for?
                if taxonTuple not in self.foundTaxa:
                    if taxonTuple not in self.unaccountedTaxonLinks:
                        self.unaccountedTaxonLinks[taxonTuple] = []
                    linkTuple = (pageTitle or '???', currentSections[2] or '???')
                    self.unaccountedTaxonLinks[taxonTuple].append(linkTuple) 
        


    def get_results(self) -> list[dict[str, str | int | None]]:
        results = []
        # sort from most-wanted to least-wanted
        for taxonTuple, linkTuples in sorted(self.unaccountedTaxonLinks.items(), key=lambda item: -len(item[1])):
            results.append({
                # 'SECTIONHEADING': taxonTuple[0][0].upper() if (len(taxonTuple[0]) >= 1 and taxonTuple[0][0].upper() >= 'A' and 
                #                                                taxonTuple[0][0].upper() <= 'Z') else 'other',
                'Page_PAGE_EDITLINK': taxonTuple[0],
                'Taxonomic rank_RAW': taxonTuple[1],
                'Link tuples_RAW': ', '.join([f'{tuple[1]} [[{tuple[0]}]]' for tuple in sorted(linkTuples, key=lambda tuple: (tuple[1], tuple[0]))])
            })

        return results
