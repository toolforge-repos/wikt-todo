import xml.etree.ElementTree
import pymysql.cursors
import toolforge
import requests
import time
import urllib.parse
import wikt_todo_utility

class EntriesWithMissingHeadwordLines:
    todoListName = 'Entries with missing headword lines'
    isUserUpdatable = True
    rowLimit = 1000

    def run(self) -> dict:
        with toolforge.connect("enwiktionary", cluster='analytics') as conn:
            results_raw = []

            cur: pymysql.cursors.SSCursor
            with conn.cursor(pymysql.cursors.SSCursor) as cur:
                # First run an SQL query to find any pages which do not have a "Pages with N entries"
                # category at all
                query = \
f"""
SELECT
    -- raw_row[0]: "Category:Pages with N entries" link
    NULL,
    -- raw_row[1] and raw_row[2]
    page_namespace,
    page_title,
    -- raw_row[3]: distinct language names taken from the front of lemma/non-lemma category names
    NULL,
    -- raw_row[4]: count of above
    0,
    -- raw_row[5]: the number N from the "Category:Pages with N entries" category
    NULL
FROM page
    LEFT JOIN categorylinks AS absent_cat ON page_id = absent_cat.cl_from AND
         -- exclude pages in a 'Pages with N entries' category
        (cl_to LIKE 'Pages\\_with\\_%\\_entr%' OR
         -- exclude {{no entry}}
         cl_to LIKE '%\\_entries\\_that\\_don_t\\_exist' OR
         -- exclude speedy deletion candidates
         cl_to = 'Candidates_for_speedy_deletion')
WHERE
    -- only keep main and Reconstruction namespace pages
    page_namespace IN (0, 118) AND
    -- skip redirects
    NOT page_is_redirect AND
    -- skip /translations and /derived_terms subpages
    page_title NOT LIKE '%/translations' AND
    page_title NOT LIKE '%/derived\\_terms' AND
    -- only return pages which are not in one of the categories of interest
    absent_cat.cl_to IS NULL
GROUP BY page_namespace, page_title
LIMIT 200   -- there shouldn't be many of these?
"""
                cur.execute(query)
                results_raw.extend(cur.fetchall())

                # Now run an SQL query to find pages where the value N in the page's
                # "Category:Pages from N entries" category doesn't match the number of
                # distinct languages reflected by the "lemmas"/"non-lemma forms" categories
                # the page is in (so-called "LNL categories")
                query = \
f"""
SELECT
    -- raw_row[0]: "Category:Pages with N entries" link
    CONCAT('[[:Category:', REPLACE(pages_with_n_entries_cat.cl_to, '_', ' '), ']]') AS pwne_cat,
    -- raw_row[1] and raw_row[2]
    page_namespace,
    page_title,
    -- raw_row[3]: distinct language names taken from the front of lemma/non-lemma category names
    GROUP_CONCAT(DISTINCT REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(lnl_cat.cl_to,
        '_lemmas', ''),
        '_non-lemma_forms', ''),
        "_entries_that_don't_exist", ''),
        '_alternative_forms', ''),
        '_', ' ') ORDER BY lnl_cat.cl_to SEPARATOR '|') AS lnl_cat_languages,
    -- raw_row[4]: count of above
    COUNT(DISTINCT REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(lnl_cat.cl_to,
        '_lemmas', ''),
        '_non-lemma_forms', ''),
        "_entries_that_don't_exist", ''),
        '_alternative_forms', ''),
        '_', ' ')) AS lnl_count,
    -- raw_row[5]: the number N from the "Category:Pages with N entries" category
    CAST(REPLACE(REPLACE(REPLACE(pages_with_n_entries_cat.cl_to, 'Pages_with_', ''), '_entry', ''), '_entries', '') AS integer) AS pwne_num
FROM page
    LEFT JOIN (templatelinks AS excluded_temp INNER JOIN linktarget AS excluded_lt ON tl_target_id = lt_id) ON
        page_id = excluded_temp.tl_from AND
        -- for the time being, completely exclude pages transcluding certain CJK cross-reference templates and {{cuns}},
        -- as well as pages with unrecognised POS tracking
        ((excluded_lt.lt_namespace = 10 AND excluded_lt.lt_title IN ('ja-gv', 'ja-kanji', 'ja-see', 'ja-see-kango', 'zh-see', 'cuns')) OR
         (excluded_lt.lt_namespace = 4 AND excluded_lt.lt_title = 'Tracking/headword/unrecognized_pos'))
    LEFT JOIN categorylinks AS excluded_cat ON
        page_id = excluded_cat.cl_from AND 
        -- also completely exclude Chinese lemmas - the logic required to properly handle them makes the query too fragile
        excluded_cat.cl_to IN ('Chinese_lemmas', 'Candidates_for_speedy_deletion')
    INNER JOIN categorylinks AS pages_with_n_entries_cat ON
        page_id = pages_with_n_entries_cat.cl_from
    LEFT JOIN categorylinks AS lnl_cat ON
        page_id = lnl_cat.cl_from AND
        -- look for lemma and non-lemma categories
        (lnl_cat.cl_to LIKE '%_lemmas' OR
        lnl_cat.cl_to LIKE '%_non-lemma_forms' OR
        lnl_cat.cl_to LIKE '%_alternative_forms' OR
        lnl_cat.cl_to LIKE '%_entries_that_don_t_exist') AND
        -- ignore categories that look like lemma categories but actually are not
        lnl_cat.cl_to NOT IN ('Old_Latin_lemmas', 'Old_Latin_non-lemma_forms') AND
        lnl_cat.cl_to NOT LIKE '%_vowel_harmonic_lemmas'
WHERE
    -- main namespace only
    page_namespace = 0 AND
    -- only keep pages that do not have one of the excluded templates
    excluded_temp.tl_from IS NULL AND
    -- only keep pages that are not in the excluded categories 
    excluded_cat.cl_from IS NULL AND
    -- look for a "Pages with N entr(y|ies)" category
    pages_with_n_entries_cat.cl_to LIKE 'Pages_with_%_entr%'
GROUP BY pages_with_n_entries_cat.cl_from
HAVING pwne_num <> lnl_count
LIMIT {self.rowLimit}
"""
                cur.execute(query)
                results_raw.extend(cur.fetchall())


        # Go through the results and determine which L2 sections on the page
        # are responsible for the mismatch
        results = []
        for raw_row in results_raw:
            # Find the list of languages represented by LNL categories on the entry
            # as returned from the SQL, excluding any which do not appear in WT:LOL
            if raw_row[3] != None:
                lnl_cat_languages = set(lang
                                        for lang in raw_row[3].decode('utf-8').split('|')
                                        if wikt_todo_utility.get_language_code(lang) != None)
            else:
                lnl_cat_languages = set()

            # Generate caption describing the situation (this is mainly to assist
            # wiki users to pinpoint the origin of any systemic problems with the list)
            if raw_row[0] == None:
                # not in any "Pages with N entries" category
                issue_caption = f'Page has no headword line at all'
            else:
                issue_caption = f"Page in {raw_row[0].decode('utf-8')} but has " + \
                    f"{raw_row[4]} relevant language categor{'y' if raw_row[4] == 1 else 'ies'}: {', '.join(sorted(lnl_cat_languages))}"

            try:
                # Get the page HTML from the REST API
                # https://en.wiktionary.org/api/rest_v1/page/html/Title
                response = requests.get('https://en.wiktionary.org/api/rest_v1/page/html/' +
                                        wikt_todo_utility.get_namespace_prefix(raw_row[1]) +
                                        urllib.parse.quote(raw_row[2].decode('utf-8'), safe=''))

                # Let the server breathe so we don't exceed 200 requests per second
                time.sleep(0.006)

                etree = xml.etree.ElementTree.fromstring(response.text)

                # Treating the HTML as XML, make a list of the <h2> headings on the page
                l2_languages = set()
                for h2El in etree.findall('.//h2'):
                    l2 = ''.join(h2El.itertext())
                    if l2:
                        l2_languages.add(l2)
            except Exception:
                results.append(('(Unknown language)', str(raw_row[1]) + '|' + raw_row[2].decode('utf-8'), issue_caption))
                continue

            # Find L2 headers with no corresponding LNL languages, and
            # add a result row corresponding to each one
            mismatched_languages = l2_languages.symmetric_difference(lnl_cat_languages)
            for langname in mismatched_languages:
                results.append((langname, str(raw_row[1]) + '|' + raw_row[2].decode('utf-8'), issue_caption))


        return sorted([{
            'SECTIONHEADING': row[0],
            'Page_NSTITLE_EDITLINK': row[1],
            'Description_RAW': row[2]
        } for row in results], key=lambda row: (row['SECTIONHEADING'], row['Page_NSTITLE_EDITLINK'].lower()))
