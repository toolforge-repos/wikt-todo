# English Wiktionary Todo Lists

This repository powers the automated todo lists at [Wiktionary:Todo/Lists](https://en.wiktionary.org/wiki/Wiktionary:Todo/Lists).

Technical documentation is available on-wiki at [Wiktionary:Todo/Lists/technical documentation](https://en.wiktionary.org/wiki/Wiktionary:Todo/Lists/technical_documentation).
