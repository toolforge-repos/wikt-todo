"""
Defines functions for generating todo lists in MediaWiki table syntax,
with values formatted appropriately.
"""


import pywikibot


# When somebody manually triggers an update of a todo list, this many hours must have elapsed
# since the last update of that list
# IMPORTANT! Keep in sync with the declaration in updater_webapp.py
MIN_HOURS_BETWEEN_UPDATE = 4


def as_string(data) -> str:
    """Convert any data value in a DB result to a string."""

    try:
        return data.decode('utf-8')
    except (UnicodeDecodeError, AttributeError):
        return str(data)


def format_data_value(colname: str, data: str) -> str:
    """Format an individual data value in an output table or list."""

    result = ''
    handled = False
    titleType = None  # NSTITLE or PAGE

    # Look for capitalised pieces of the column name. These indicate
    # how the column is supposed to be converted to wikitext
    colname_pieces = colname.split('_')
    for piece in colname_pieces:

        if piece.upper() == piece:
            # Link to a page in namespace-title format
            # Examples: 0|dictionary    3|This,_that_and_the_other
            # The SQL should be CONCAT(page_namespace, '|', page_title) or equivalent
            if piece == 'NSTITLE':
                nsAndTitle = data.split('|', 2)
                nsAndTitle[1] = nsAndTitle[1].replace('_', ' ')
                result += '[[:{{subst:NS:%s}}:%s]]' % (nsAndTitle[0], nsAndTitle[1]) if nsAndTitle[0] != '0' else '[[:%s]]' % nsAndTitle[1]
                titleType = 'NSTITLE'
                handled = True

            # Link to a regular page title, stripping any anchors from the visible link
            # Examples: dictionary    User:This,_that_and_the_other    papa#French
            if piece == 'PAGE':
                if '#' in data:
                    result += '[[:%s|%s]]' % (data.replace('_', ' '), data.replace('_', ' ').split('#', 1)[0])
                else:
                    result += '[[:%s]]' % data.replace('_', ' ')
                titleType = 'PAGE'
                handled = True

            # "Edit" link to a page previously specified by a NSTITLE or PAGE code
            if piece == 'TALKLINK':
                if titleType == 'NSTITLE':
                    nsAndTitle = data.split('|', 2)
                    # only add this link for non-talk (even-numbered) namespaces
                    if int(nsAndTitle[0]) % 2 == 0:
                        nsAndTitle[1] = nsAndTitle[1].replace('_', ' ')
                        result += ' ([[%s|talk]])' % \
                            ('{{subst:NS:%s}} talk:%s' % (nsAndTitle[0], nsAndTitle[1]) if nsAndTitle[0] != '0' else 'Talk:%s' % nsAndTitle[1])
                elif titleType == 'PAGE':
                    result += ' ([[{{subst:TALKPAGENAME:%s}}|talk]])' % data.replace('_', ' ').split('#', 1)[0]
                else:
                    raise RuntimeError('The TALKLINK formatting code can only be used after NSTITLE or PAGE')
                handled = True

            # "Edit" link to a page previously specified by a NSTITLE or PAGE code
            if piece == 'EDITLINK':
                if titleType == 'NSTITLE':
                    nsAndTitle = data.split('|', 2)
                    nsAndTitle[1] = nsAndTitle[1].replace('_', ' ')
                    result += ' ([[Special:Edit/%s|edit]])' % \
                        ('{{subst:NS:%s}}:%s' % (nsAndTitle[0], nsAndTitle[1]) if nsAndTitle[0] != '0' else nsAndTitle[1])
                elif titleType == 'PAGE':
                    result += ' ([[Special:Edit/%s|edit]])' % data.replace('_', ' ').split('#', 1)[0]
                else:
                    raise RuntimeError('The EDITLINK formatting code can only be used after NSTITLE or PAGE')
                handled = True

            # "Edit" link to a page previously specified by a NSTITLE or PAGE code
            if piece == 'HISTLINK':
                if titleType == 'NSTITLE':
                    nsAndTitle = data.split('|', 2)
                    nsAndTitle[1] = nsAndTitle[1].replace('_', ' ')
                    result += ' ([[Special:PageHistory/%s|history]])' % \
                        ('{{subst:NS:%s}}:%s' % (nsAndTitle[0], nsAndTitle[1]) if nsAndTitle[0] != '0' else nsAndTitle[1])
                elif titleType == 'PAGE':
                    result += ' ([[Special:PageHistory/%s|history]])' % data.replace('_', ' ').split('#', 1)[0]
                else:
                    raise RuntimeError('The HISTLINK formatting code can only be used after NSTITLE or PAGE')
                handled = True

            # Raw wikitext
            # Use this if you need to substitute magic words, like {{subst:NS:...}},
            # or templates, like {{subst:langname|...}}, into the output
            if piece == 'RAW':
                result += data
                handled = True

            # Wrap in <nowiki> tags
            if piece == 'NOWIKI':
                result += '<nowiki>%s</nowiki>' % data
                handled = True
            
            # Wrap in <code><nowiki> tags
            if piece == 'CODE':
                result += '<code><nowiki>%s</nowiki></code>' % data
                handled = True
            
            # Wrap in <code><nowiki> tags, keeping only the first 50 characters and adding ... if more
            if piece == 'CODE50':
                result += f'<code><nowiki>{data[0:50]}</nowiki></code>{"..." if len(data) > 50 else ""}'
                handled = True
            
            # Wrap in <code><nowiki> tags, keeping only the first 100 characters and adding ... if more
            if piece == 'CODE100':
                result += f'<code><nowiki>{data[0:100]}</nowiki></code>{"..." if len(data) > 100 else ""}'
                handled = True
        
    if not handled:
        raise RuntimeError('Column name lacks a formatting code: %s' % colname)
    
    return result


def format_header_cell_value(colname: str) -> str:
    """Generate the content of a header cell"""

    words = []

    colname_pieces = colname.split('_')
    for piece in colname_pieces:
        # Skip over uppercase pieces, which are special codes
        if piece.upper() == piece:
            continue
        words.append(piece)

    return ' '.join(words)


def generate_table_start(colnames: list) -> str:
    """
    Helper function to generate the table or list opening, and header row if applicable.
    """

    result = ''

    # If there is only one key besides SECTIONHEADING, we are generating
    # a bulleted list, which does not need a heading
    if len(colnames) > 1: 
        result = '{| class="wikitable sortable"\n'
        result += '! ' + ' !! '.join(format_header_cell_value(as_string(colname)) for colname in colnames if colname != "SECTIONHEADING")
        result += '\n'

    return result


class OutputFormatter:
    def __init__(self, updateFreq = None, isUserUpdatable = False, rowLimit = 5000):
        # PARAMETERS

        # The automatic update frequency of this list (human-readable free text)
        self.updateFreq = updateFreq
        # Can this list be updated by the user?
        self.isUserUpdatable = isUserUpdatable
        # The maximum number of rows that should be output
        self.rowLimit = rowLimit

        # STATE VARIABLES

        # The column names for the first row of data. All other column
        # names must match this
        self.colnames = None
        # The current section heading
        self.currentSectionHeading = None
        # The set of column names
        self.colnames = None
        # Failure output, if any
        self.failureOutput = None
        # Has the row limit been exceeded?
        self.rowLimitExceeded = False

        # OUTPUT DATA

        # Output wikitext so far. Do not access from outside the class!
        self._output = ''
        # Number of rows written
        self.rowCount = 0


    def handle_row(self, row: dict) -> bool:
        """
        Given a row dictionary, generates a row of the output table or item in the bulleted list.

        Returns False if the row limit has been reached, otherwise True.
        """

        if self.rowLimitExceeded:
            return False
        
        if self.rowCount == self.rowLimit:
            self.rowLimitExceeded = True
            return False

        # If this is the first row, store column names
        if not self.colnames:
            self.colnames = list(row.keys())
            if "SECTIONHEADING" in self.colnames:
                self.colnames.remove("SECTIONHEADING")

        # Look for the magical column name SECTIONHEADING
        newSectionHeading = None
        if "SECTIONHEADING" in row:
            newSectionHeading = as_string(row["SECTIONHEADING"])
            row.pop("SECTIONHEADING")

            # Do we need to generate a section header?
            if self.currentSectionHeading != newSectionHeading:
                if len(self._output) > 0:
                    if len(self.colnames) > 1:
                        self._output += '|}\n'
                    self._output += '\n'
                self._output += '====%s====\n\n' % ('(empty)' if newSectionHeading == '' else ("''(null)''" if newSectionHeading == None else newSectionHeading))
                self.currentSectionHeading = newSectionHeading
                self._output += generate_table_start(self.colnames)

        # Write a table header if it hasn't yet been written
        if self._output == '':
            self._output = generate_table_start(self.colnames)

        # If there is only one key, output a bulleted list
        if len(self.colnames) == 1:
            self._output += '* ' + ''.join(
                    ("''null''" if data == None else format_data_value(as_string(colname), as_string(data))) for colname, data in row.items()
                ) + '\n'
        else:
            # Otherwise generate a table row
            self._output += '|-\n'
            self._output += '| ' + ' || '.join(
                    ("''null''" if data == None else format_data_value(as_string(colname), as_string(data))) for colname, data in row.items()
                ) + '\n'

        self.rowCount += 1

        return True


    def get_wikitext(self) -> str:
        """
        Gets the wikitext output of this output formatter.
        """
        
        if self.rowLimitExceeded:
            numResultsText = 'More than %d results; only the first <section begin="numresults" />%d results<section end="numresults" /> are shown:' % (self.rowCount, self.rowCount)
        elif self.rowCount == 0:
            numResultsText = '<section begin="numresults" />No results<section end="numresults" />.'
        else: 
            numResultsText = '<section begin="numresults" />%d result%s<section end="numresults" />:' % (self.rowCount, '' if self.rowCount == 1 else 's')

        result = \
"""{{/description}}
[[Category:Todo lists|{{SUBPAGENAME}}]]
    
----

%s===The list===

Last updated <section begin="lastupdated" />{{subst:#time:H:i, d F Y}} (UTC)<section end="lastupdated" />.%s%s

%s

%s""" % (
    '__TOC__\n\n' if self.currentSectionHeading != None else '',
    (' Automatic updates occur %s' % self.updateFreq) if self.updateFreq else '',
    (', or {{clickable button|Update now|url=https://wikt-todo.toolforge.org/updater/update/{{subst:urlencode:{{subst:SUBPAGENAME}}|PATH}}|class=mw-ui-progressive}} ' +
      '(no more than once every %d hours)' % MIN_HOURS_BETWEEN_UPDATE) if self.isUserUpdatable else '.',
    numResultsText,
    self._output
)
                
        # Do we need to output a final closing table bracket?
        if len(self._output) > 0 and len(self.colnames) > 1:
            result += '|}'
        
        return result
    

    def save_output_to_wiki(self, todoListName: str, userName: str | None = None) -> None:
        # Save the output to a wiki page
        site = pywikibot.Site()
        page = pywikibot.Page(site, "Wiktionary:Todo/Lists/%s" % todoListName)
        page.text = self.get_wikitext()
        page.save(f"Update todo list: {self.rowCount} result{'' if self.rowCount == 1 else 's'}" +
                  ('' if userName == None else f', initiated by [[User:{userName}|{userName}]]'), quiet=True, botflag=False)

