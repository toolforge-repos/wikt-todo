from datetime import datetime
import sys
import traceback
import xml.sax
import bz2
import re
import mwparserfromhell

# Also import relevant local modules
from output_formatting import OutputFormatter
import wikt_todo_utility

# Only generate the first 5000 results for each query
ROW_LIMIT = 5000



# Import the todo list classes from the xmldump directory
todoListClasses = wikt_todo_utility.get_todo_list_classes_from_directory('xmldump')

# Get command line arguments
args = wikt_todo_utility.get_command_line_args(queryNames=[todoListObj.todoListName for todoListObj in todoListClasses], allowLocalFile=True)

# If a specific todo list was requested, only run that one
if args.name != None:
    todoListClasses = [todoListClass for todoListClass in todoListClasses if todoListClass.todoListName == args.name]
    if len(todoListClasses) == 0:
        print('invalid todo list class - should have been caught by the command line parser!', file=sys.stderr)
        exit(1)



# Regexes for identifying wikitext headings and comments
headingRegex = re.compile(r'^(={1,6})\s*(.+?)\s*\1\s*$')
commentRegex = re.compile(r'<!--.*?-->')

class MWDumpHandler(xml.sax.ContentHandler):
    """
    A low-level xml.sax handler that reads the content of each page of a MediaWiki XML dump,
    line by line. Designed to be used on `pages-articles` and `pages-meta-current` dumps.
    """

    def __init__(self):
        # The set of todo list objects associated to this handler
        self.todoLists = []

        # The current XML tag being processed
        self.tagName = None
        # Are we inside a <revision> tag?
        self.inRevision = False

        # The page ID of the page currently being processed
        self.pageId = ''
        # The title of the page currently being processed
        self.pageTitle = ''
        # The namespace of the page currently being processed
        self.pageNs = ''
        # The current line of content of the page so far. This will be incrementally
        # built up as chunks are read from the XML file.
        self.currentContent = ''
        # A dictionary containing the heading of the current section heading at each
        # level from 1 to 6. This respects section hierarchy, that is, when a level 2
        # heading is encountered, the level 3, 4, 5 and 6 headings are all set to None,
        # and so on.
        self.currentSections = { 1: None, 2: None, 3: None, 4: None, 5: None, 6: None }
        # Are we inside a comment?
        self.inComment = False

        # Keep track of the last page ID printed
        self.lastPrintedPageId = -1
    
    def callCallbacks(self):
        """Calls all todo list callbacks attached to this handler and store any result row."""

        # If the line contains the sequence '{{', supply a parsed version of this line
        currentContentParsed = None
        if '{{' in self.currentContent:
            currentContentParsed = mwparserfromhell.parse(self.currentContent, skip_style_tags=True)

        # Make sure there is at least one non-failed todo list
        anyNonFailedLists = False

        for todoListObj in self.todoLists:
            if not todoListObj.failed:
                try:
                    todoListObj.handle_line(pageId=self.pageId,
                                            pageNs=self.pageNs,
                                            pageTitle=self.pageTitle,
                                            currentSections=self.currentSections,
                                            line=self.currentContent,
                                            lineParsed=currentContentParsed)
                    anyNonFailedLists = True
                except Exception as e:
                    print(todoListObj.todoListName, 'FAILED: ', ''.join(traceback.format_exception(e)), file=sys.stderr)
                    # Mark this todo list object as failed
                    todoListObj.failed = True
        
        if not anyNonFailedLists:
            print('all todo list objects failed, exiting', file=sys.stderr)
            exit(1)

    def startDocument(self):
        """Called by xml.sax at the beginning of its work."""
        self.todoLists = []
        for todoListClass in todoListClasses:
            try:
                todoListObj = todoListClass()
                todoListObj.failed = False
                self.todoLists.append(todoListObj)
            except Exception as e:
                print(todoListClass.todoListName, 'FAILED DURING INITIALISATION: ', ''.join(traceback.format_exception(e)), file=sys.stderr)
        
        for todoListObj in self.todoLists:
            todoListObj.failed = False

    def startElement(self, tag, attributes):
        """Called by xml.sax when an element starts."""
        self.tagName = tag

        if tag == 'revision':
            # We have entered the <revision> tag
            self.inRevision = True
        elif tag == 'page':
            # We are starting a new page
            self.pageId = ''
            self.pageTitle = ''
            self.pageNs = ''

    def endElement(self, tag):
        """Called by xml.sax when an element ends."""
        self.tagName = None
        if tag == "id" and not self.inRevision:
            self.pageId = int(self.pageId)
            
            # Print the page ID every 200000 pages (200000 -> 0.2M, 400000 -> 0.4M, ...)
            if self.lastPrintedPageId // 200000 != self.pageId // 200000:
                print(f'{(self.pageId // 200000) / 5}M..', end='', flush=True)
                self.lastPrintedPageId = int(self.pageId)
        elif tag == "ns":
            self.pageNs = int(self.pageNs)
        elif tag == 'revision':
            # We have left the <revision> tag
            self.inRevision = False

            if self.currentContent != '':
                # The last line needs to be handled. Call the callback functions
                self.callCallbacks()

            self.currentSections = { 1: None, 2: None, 3: None, 4: None, 5: None, 6: None }
            self.currentContent = ''
            self.inComment = False

    def characters(self, content):
        """Called by xml.sax when a line of text is read."""
        if self.tagName == "id" and not self.inRevision:
            self.pageId += content  # converted to int in the endElement() function
        elif self.tagName == "title":
            self.pageTitle += content
        elif self.tagName == "ns":
            self.pageNs += content  # converted to int in the endElement() function
        elif self.tagName == "text" and (self.pageNs == 0 or self.pageNs == 118):  # Main, Reconstruction
            self.currentContent = self.currentContent + content
            
            if self.currentContent.endswith('\n'):
                # Are we in the middle of an unclosed comment?
                if self.inComment:
                    # Is the comment closed on this line?
                    if '-->' in self.currentContent:
                        self.inComment = False
                        self.currentContent = self.currentContent.split('-->', 1)[1]
                
                # Are we outside an unclosed comment?
                if not self.inComment:
                    # Strip the trailing newline and any intermediate comments
                    self.currentContent = commentRegex.sub('', self.currentContent[:-1])

                    # Does the string contain an unclosed comment?
                    # TODO This doesn't play well when <!-- is inside a nowiki tag, but this situation
                    # should be extremely rare and we can deal with it when/if it causes problems
                    if '<!--' in self.currentContent:
                        self.inComment = True
                        self.currentContent = self.currentContent.split('<!--', 1)[0]

                    # Update current section heading
                    if self.currentContent.startswith('='):
                        headingMatch = headingRegex.match(self.currentContent)
                        if headingMatch != None:
                            headingLevel = len(headingMatch.group(1))
                            self.currentSections[headingLevel] = headingMatch.group(2).strip()
                            for i in range(headingLevel + 1, 7):
                                self.currentSections[i] = None

                    # Call the callback functions
                    self.callCallbacks()

                self.currentContent = ''



# Run the dump processor and capture output
handler = MWDumpHandler()

file = '/public/dumps/public/enwiktionary/latest/enwiktionary-latest-pages-meta-current.xml.bz2'
if args.file:
    file = args.file

if file.endswith('.bz2'):
    with bz2.open(file, 'rt', encoding='utf-8') as bz2file:
        xml.sax.parse(bz2file, handler)
else:
    with open(file, 'rt', encoding='utf-8') as file:
        xml.sax.parse(file, handler)

for todoListObj in handler.todoLists:
    # Is this todo list the one requested by the user?
    if args.name != None and args.name != todoListObj.todoListName:
        continue
    
    # Has this todo list failed?
    if todoListObj.failed:
        # Error message was already printed, no need for another one
        continue

    output = OutputFormatter(updateFreq='around the 3rd and 22nd of every month', rowLimit=ROW_LIMIT)

    try:
        todoListResults = todoListObj.get_results()
    except Exception as e:
        print(todoListObj.todoListName, 'FAILED WHEN GETTING RESULTS: ', ''.join(traceback.format_exception(e)), file=sys.stderr)
        continue

    for row in todoListResults:
        if not output.handle_row(row):
            break

    if args.dry_run:
        print(output.get_wikitext())
    else:
        output.save_output_to_wiki(todoListObj.todoListName)

    print('%s: updated xmldump list %s: %s' % (datetime.now().replace(microsecond=0).isoformat(),
                                               todoListObj.todoListName,
                                               '%d results' % output.rowCount))


