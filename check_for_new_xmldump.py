"""
Checks for a new XML dump and, if one is found, runs generate_lists_xmldump
"""

import os
import time

dumpFile = '/public/dumps/public/enwiktionary/latest/enwiktionary-latest-pages-meta-current.xml.bz2'
lastDumpTimeFile = 'last-xmldump-time.txt'

currentDumpTime = int(os.path.getmtime(dumpFile))
lastDumpTime = int(open(lastDumpTimeFile, 'r').read().strip())

if currentDumpTime > lastDumpTime:
    # Run the dump processor
    errorCode = os.system('~/pyvenv/bin/python src/generate_lists_xmldump.py --all')

    # Update the txt file with the new last modified time
    open(lastDumpTimeFile, 'w').write(str(currentDumpTime))

    # Mimic the error code of the subprocess
    exit(errorCode)


exit(0)
