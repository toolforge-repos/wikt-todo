"""
Web-facing script to serve the "Update now" buttons on todo list pages.
OAuth is used to make sure random Internet users/trolls/bots aren't able to
trigger this functionality.

This script is symlinked from $HOME/www/python/src/app.py
"""

# Based on the Toolforge Flask + OAuth WSGI tutorial
# https://wikitech.wikimedia.org/wiki/Help:Toolforge/My_first_Flask_OAuth_tool
#
# Copyright (C) 2017 Bryan Davis and contributors
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along
# with this program.  If not, see <http://www.gnu.org/licenses/>.

import flask
import mwoauth
import yaml
import os
import subprocess
from markupsafe import escape
from datetime import datetime


# When somebody manually triggers an update of a todo list, this many hours must have elapsed
# since the last update of that list
# IMPORTANT! Keep in sync with the declaration in output_formatting.py
MIN_HOURS_BETWEEN_UPDATE = 4


# Import SQL query definitions
import sys
sys.path.append(os.environ['HOME'] + '/src')
from sql.queries import queries


app = flask.Flask(__name__)

PAGE_TITLE = '<title>English Wiktionary Todo List Updater</title><h1>English Wiktionary Todo List Updater</h1>'

# Keep track of which lists are being updated, to avoid race conditions where
# two users simultaneously start updating the same list
list_update_times = dict()


def can_update_list(list_type, list_name):
    """Is it too soon to update this list? That is, was it last updated less than MIN_HOURS_BETWEEN_UPDATE hours ago?"""

    list_last_updated_file = f"{os.environ['HOME']}/last-{list_type}-times/{list_name}"

    try:
        list_last_updated_time = int(float(open(list_last_updated_file, 'r').read().strip()))
    except FileNotFoundError:
        list_last_updated_time = 0

    list_earliest_update_time = list_last_updated_time + 60 * 60 * MIN_HOURS_BETWEEN_UPDATE
    current_time = datetime.utcnow().timestamp()

    if list_earliest_update_time > current_time:
        # Too soon to update
        return \
f"""
The list <b>{escape(list_name)}</b> was updated within the last {MIN_HOURS_BETWEEN_UPDATE} hours.
A limit of one update per {MIN_HOURS_BETWEEN_UPDATE} hours applies to every todo list.

<table>
<tr><td>List last updated:</td><td>{datetime.fromtimestamp(list_last_updated_time)} UTC</td></tr>
<tr><td>The time now is:</td><td>{datetime.fromtimestamp(int(current_time))} UTC</td></tr>
<tr><td>Try again after:</td><td>{datetime.fromtimestamp(list_earliest_update_time)} UTC</td></tr>
</table>
"""
    elif list_name in list_update_times and list_update_times[list_name] > current_time - 60 * 60:
        # An update of this list was commenced within the last hour and has not finished
        # (if it had finished, the if statement above would have triggered)
        return f"An update of the list <b>{escape(list_name)}</b> is already underway. Check the list's page in a few minutes to see the updated list."
    else:
        # All good, proceed
        return False



# Load configuration from YAML file
__dir__ = os.path.dirname(__file__)
app.config.update(
    yaml.safe_load(open(os.path.join(__dir__, 'config.yaml'))))
    

@app.route('/')
@app.route('/updater')
def index():
    return PAGE_TITLE + \
"""
This service is responsible for keeping Wiktionary's <a href="https://en.wiktionary.org/wiki/WT:Todo/Lists">Todo Lists</a> up to date.
<p>The service runs automatically on a schedule, and can also be invoked manually by clicking the "Update now" button on any todo list.
<p>Contact the maintainers for more information: <a href="https://en.wiktionary.org/wiki/Wiktionary_talk:Todo/Lists">Wiktionary talk:Todo/Lists</a>.
"""


CUSTOM_TODO_LIST_NAME_PREFIX = "    todoListName = '"
CUSTOM_TODO_LIST_UPDATABLE_PREFIX = "    isUserUpdatable = "

@app.route('/updater/update/<string:list_name>')
def update(list_name):
    """Commences the process of updating the requested todo list."""

    # Check that the list exists and find out what type of list it is
    if list_name in queries:
        list_type = 'sql'
    else:
        # It's not an SQL list, but it might be a custom list, so check all those lists
        # to see if their name matches
        # (It would be nice to reuse wikt_todo_utility.get_todo_list_classes_from_directory
        # here, but you then run into issues with libraries - this web app would need to
        # have the same set of libraries installed as the todo list generator scripts...)
        # NOTE: this code assumes `todoListName` comes BEFORE `isUserUpdatable`
        custom_list_is_user_updatable = None
        for filename in os.listdir(os.environ['HOME'] + '/src/custom'):
            found_custom_list_name = None
            if filename.endswith('.py') or filename == '!template.py':
                with open(os.environ['HOME'] + '/src/custom/' + filename, 'r', encoding='utf-8') as file:
                    for line in file:
                        if line.startswith(CUSTOM_TODO_LIST_NAME_PREFIX) and list_name == line[len(CUSTOM_TODO_LIST_NAME_PREFIX):-2]:
                            found_custom_list_name = True
                        elif line.startswith(CUSTOM_TODO_LIST_UPDATABLE_PREFIX):
                            custom_list_is_user_updatable = line.startswith(CUSTOM_TODO_LIST_UPDATABLE_PREFIX + 'True')
                            
                        if found_custom_list_name and (custom_list_is_user_updatable != None):
                            break
                        
            if custom_list_is_user_updatable != None:
                break
        
        if custom_list_is_user_updatable:
            list_type = 'custom'
        else:
            return PAGE_TITLE + 'The list <b>%s</b> does not exist or cannot be updated on demand.' % escape(list_name), 404

    # Check that the list hasn't been updated within the last MIN_HOURS_BETWEEN_UPDATE hours
    can_update_result = can_update_list(list_type, list_name)
    if can_update_result:
        return PAGE_TITLE + can_update_result, 403

    # Initiate an OAuth login.
    # Call the MediaWiki server to get request secrets and then redirect the
    # user to the MediaWiki server to sign the request.
    
    consumer_token = mwoauth.ConsumerToken(
        app.config['CONSUMER_KEY'], app.config['CONSUMER_SECRET'])
    try:
        redirect, request_token = mwoauth.initiate(
            app.config['OAUTH_MWURI'], consumer_token)
    except Exception:
        flask.session.clear()
        return PAGE_TITLE + 'OAuth initiation experienced an error. Please ask one of the project maintainers to inspect the error log.', 500
    
    else:
        flask.session['request_token'] = dict(zip(
            request_token._fields, request_token))
        flask.session['requested_list_type'] = list_type
        flask.session['requested_list_name'] = list_name
        return flask.redirect(redirect)


@app.route('/updater/oauth-callback')
def oauth_callback():
    """OAuth handshake callback."""

    if (
        'request_token' not in flask.session or
        'requested_list_type' not in flask.session or
        'requested_list_name' not in flask.session
    ):
        flask.session.clear()
        return PAGE_TITLE + 'Required cookies are missing. Please make sure cookies are enabled in your web browser. Otherwise, you cannot use this service.', 400
    list_type = flask.session['requested_list_type']
    list_name = flask.session['requested_list_name']

    consumer_token = mwoauth.ConsumerToken(
        app.config['CONSUMER_KEY'], app.config['CONSUMER_SECRET'])

    try:
        access_token = mwoauth.complete(
            app.config['OAUTH_MWURI'],
            consumer_token,
            mwoauth.RequestToken(**flask.session['request_token']),
            flask.request.query_string)

        identity = mwoauth.identify(
            app.config['OAUTH_MWURI'], consumer_token, access_token)  
    except Exception:
        flask.session.clear()
        return PAGE_TITLE + 'OAuth handshake experienced an error. Please ask one of the project maintainers to inspect the error log.', 500
    
    # To avoid race conditions, make sure the requested list still can be updated
    can_update_result = can_update_list(list_type, list_name)
    if can_update_result:
        flask.session.clear()
        return PAGE_TITLE + can_update_result, 403

    # Success! Go ahead and refresh the todo list
    list_update_times[list_name] = datetime.utcnow().timestamp()

    # Open a log file to store output. Doesn't matter if it is in use, we will just not log anything
    try:
        logfile = open(os.environ['HOME'] + '/ad-hoc-updates.log', 'a')
    except:
        logfile = None
    os.chdir(os.environ['HOME'])
    subprocess.Popen([os.environ['HOME'] + '/pyvenv/bin/python',
                      f'./src/generate_lists_{list_type}.py',
                      '--user', identity['username'],
                      list_name],
                     stderr=subprocess.STDOUT if logfile else None, stdout=logfile)

    return flask.redirect(flask.url_for('success'))


@app.route('/updater/success')
def success():
    """Clear the session and print a success message."""

    if 'requested_list_name' not in flask.session:
        flask.session.clear()
        return flask.redirect(flask.url_for('index'))
    
    list_name = flask.session['requested_list_name']
    flask.session.clear()
    return PAGE_TITLE + \
"""
The server has begun generating the todo list <b><a href="https://en.wiktionary.org/wiki/Wiktionary:Todo/Lists/%s">%s</a></b>.
Check back there in a few minutes to see the updated list.
""" % (list_name.replace(' ', '_'), list_name)

