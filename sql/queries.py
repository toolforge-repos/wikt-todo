"""
This file defines the SQL queries for generating SQL-based todo lists.
"""

# Namespaces that contain actual reader-facing wiki content
CONTENT_NAMESPACES = [
    '0',   # main
    '12',  # Help
    '14',  # Category
    '100', # Appendix
    '106', # Rhymes
    '110', # Thesaurus
    '114', # Citations
    '116', # Sign gloss
    '118', # Reconstruction
]


# Generate a custom sort order for non-talk namespaces: Main, Reconstruction,
# then all other reader-facing content namespaces in alphabetical order,
# then all the built-in namespaces
def namespaceOrderBy(namespaceColumn: str) -> str:
    nsOrder = [
        '0',   # main
        '118', # Reconstruction
        '100', # Appendix
        '14',  # Category
        '114', # Citations
        '12',  # Help
        '106', # Rhymes
        '116', # Sign gloss
        '110', # Thesaurus
        # ... all other namespaces
    ]
    nsList = ','.join(nsOrder)
    # The MySQL FIELD function returns the index of its first parameter in the provided
    # list of values. It returns 0 if the supplied value is not found. We want to place
    # all other namespaces in order of namespace ID at the end of the list, which is
    # achieved using the NULLIF function to replace 0 with NULL, and COALESCE to replace
    # this NULL with a large number that will sort after the namespaces listed above.
    return f'COALESCE(NULLIF(FIELD({namespaceColumn}, {nsList}), 0), 10000 + {namespaceColumn})'


# Generate a section heading field that shows (Main) for the main namespace,
# and the namespace name in all other cases
def namespaceToSectionHeading(namespaceColumn: str) -> str:
    return f"CASE WHEN {namespaceColumn} = 0 THEN '(Main)' ELSE CONCAT('{{{{subst:NS:', {namespaceColumn}, '}}}}') END"


# Generate SQL that extracts the date from the provided MediaWiki timestamp
# as a string: yyyy-mm-dd
def date(timestampColumn: str) -> str:
    # Note these hyphens are U+2011 NON-BREAKING HYPHEN, not the ordinary hyphen
    return f"CONCAT(SUBSTRING({timestampColumn}, 1, 4), '‑', SUBSTRING({timestampColumn}, 5, 2), '‑', SUBSTRING({timestampColumn}, 7, 2))"


# List of queries to be executed
# Do not include LIMIT/OFFSET clauses in these queries
# Do not end the query with a semicolon
queries = {
    

"Uncategorised pages (all namespaces)":
f"""
SELECT {namespaceToSectionHeading('page_namespace')} AS SECTIONHEADING,
       CONCAT(page_namespace, '|', page_title) AS Page_NSTITLE_EDITLINK
FROM page
  LEFT JOIN categorylinks ON page_id = cl_from
WHERE
  -- skip redirects
  NOT page_is_redirect AND
  -- content namespaces only (even-numbered namespaces), skip all talk pages
  page_namespace % 2 = 0 AND
  -- skip over the User, MediaWiki and LiquidThreads namespaces
  page_namespace NOT IN (2, 8, 90, 92) AND
  -- skip all subpages in the Wiktionary namespace (there is too much junk cluttering the list)
  NOT (page_namespace = 4 AND page_title LIKE '%/%') AND
  -- skip all subpages and user sandboxes in Template space
  NOT (page_namespace = 10 AND (page_title LIKE '%/%' OR
                                page_title LIKE 'User:%')) AND
  -- skip /sandbox, /testcases, /documentation and TemplateStyles pages as well as user sandboxes in Module space
  NOT (page_namespace = 828 AND (page_title LIKE '%/sandbox' OR
                                 page_title LIKE '%/testcases' OR
                                 page_title LIKE '%/documentation' OR
                                 page_title LIKE '%.css' OR
                                 page_title LIKE 'User:%')) AND
  -- only return pages with no entries in `categorylinks`
  cl_from IS NULL
ORDER BY {namespaceOrderBy('page_namespace')}, page_title
""",


"Entries using nonexistent templates":
f"""
SELECT {namespaceToSectionHeading('source_page.page_namespace')} AS SECTIONHEADING,
       CONCAT(source_page.page_namespace, '|', source_page.page_title) AS Page_NSTITLE_EDITLINK,
       CASE WHEN lt_namespace = 10
            THEN lt_title
            ELSE CONCAT('{{{{subst:NS:', lt_namespace, '}}}}:', lt_title)
       END AS Template_RAW
FROM templatelinks
  INNER JOIN page AS source_page ON tl_from = source_page.page_id
  INNER JOIN linktarget ON tl_target_id = lt_id
  LEFT JOIN page AS transcluded_page ON lt_namespace = transcluded_page.page_namespace AND lt_title = transcluded_page.page_title
WHERE
  -- only return transclusions of templates
  lt_namespace = 10 AND 
  -- only return faulty transclusions on content pages
  source_page.page_namespace IN ({','.join(CONTENT_NAMESPACES)}) AND
  -- skip transclusions of Template:tracking/... pages
  NOT (lt_namespace = 10 AND lt_title LIKE 'tracking/%') AND
  -- only return pages where the template doesn't exist
  transcluded_page.page_id IS NULL
ORDER BY {namespaceOrderBy('source_page.page_namespace')}, lt_namespace, LOWER(lt_title), LOWER(source_page.page_title)
""",


"Entries linking to raw template syntax": 
f"""
SELECT CONCAT(source_page.page_namespace, '|', source_page.page_title) AS Page_NSTITLE_EDITLINK,
       CASE WHEN COUNT(DISTINCT lt_title) > 1
            THEN CONCAT(SUBSTRING_INDEX(GROUP_CONCAT(DISTINCT lt_title ORDER BY lt_title SEPARATOR '|'), '|', 1), ' and ', COUNT(DISTINCT lt_title) - 1, ' others')
            ELSE GROUP_CONCAT(DISTINCT lt_title ORDER BY lt_title)
       END AS Links_to_NOWIKI,
       GROUP_CONCAT(DISTINCT REPLACE(REPLACE(REPLACE(cl_to, '_lemmas', ''), '_non-lemma_forms', ''), '_', ' ') ORDER BY cl_to SEPARATOR ', ') AS Languages_on_page_RAW
FROM pagelinks
  INNER JOIN linktarget ON pl_target_id = lt_id
  INNER JOIN page AS source_page ON pl_from = source_page.page_id
  LEFT JOIN categorylinks ON pl_from = cl_from
  LEFT JOIN page AS linked_page ON lt_namespace = linked_page.page_namespace AND lt_title = linked_page.page_title
WHERE
  -- find links to titles beginning with two curly braces
  lt_namespace = 0 AND
  lt_title LIKE 'Unsupported_titles/`lcub``lcub`%' AND
  -- only return faulty links on content pages
  source_page.page_namespace IN ({','.join(CONTENT_NAMESPACES)}) AND
  -- only return pages where the linked page doesn't exist
  linked_page.page_id IS NULL AND
  -- only return categories that end in "lemmas" or "non-lemma forms", to establish what L2 language(s) the entry contains
  (cl_to LIKE '%_lemmas' OR cl_to LIKE '%_non-lemma_forms')
GROUP BY Page_NSTITLE_EDITLINK
ORDER BY Languages_on_page_RAW, {namespaceOrderBy('source_page.page_namespace')}, source_page.page_title
""",


# "Orphaned Reconstruction pages":
# """
# SELECT CONCAT(page_namespace, '|', page_title) AS Page_NSTITLE
# FROM page LEFT JOIN pagelinks ON page_namespace = pl_namespace AND page_title = pl_title AND
#                                  -- only consider links from reader-facing content pages
#                                  pl_from_namespace IN (""" + ','.join(CONTENT_NAMESPACES) + """)
# WHERE
#   -- only return pages in the Reconstruction namespace
#   page_namespace = 118 AND
#   -- only return pages with no incoming links from content pages
#   pl_from IS NULL
# ORDER BY page_title
# """,


"Blank or extremely short pages":
f"""
SELECT {namespaceToSectionHeading('page_namespace')} AS SECTIONHEADING,
       CONCAT(page_namespace, '|', page_title) AS Page_NSTITLE_EDITLINK,
       page_len as Length_in_bytes_RAW
FROM page
  LEFT JOIN categorylinks ON page_id = cl_from AND cl_to = 'Candidates_for_speedy_deletion'
WHERE
  -- return pages with length <= 20 bytes, except for categories, where the threshold is 10 bytes (the text "{{autocat}}" is 11 bytes)
  CASE WHEN page_namespace = 14
       THEN page_len <= 10
       ELSE page_len <= 20
  END AND
  -- skip redirects
  NOT page_is_redirect AND
  -- content namespaces only (even-numbered namespaces), skip all talk pages
  page_namespace % 2 = 0 AND
  -- skip over the User, MediaWiki and LiquidThreads namespaces
  page_namespace NOT IN (2, 8, 90, 92) AND
  -- skip archive pages in the Wiktionary namespace (whether 'Archive' or 'archive')
  NOT (page_namespace = 4 AND page_title LIKE '%rchive%') AND
  -- skip all subpages and user sandboxes in Template space
  NOT (page_namespace = 10 AND (page_title LIKE '%/%' OR
                                page_title LIKE 'User:%')) AND
  -- skip /sandbox, /testcases and user sandboxes in Module space
  NOT (page_namespace = 828 AND (page_title LIKE '%/sandbox' OR
                                 page_title LIKE '%/testcases' OR
                                 page_title LIKE 'User:%')) AND
  -- skip pages marked for speedy deletion
  cl_to IS NULL
ORDER BY {namespaceOrderBy('page_namespace')}, page_len, page_title
""",


"RFVs and RFDs tagged but not listed":
f"""
SELECT CASE WHEN cl_to LIKE 'Requests_for_verification_in_%'
            THEN 'RFV'
            WHEN cl_to LIKE 'Requests_for_deletion_in_%'
            THEN 'RFD'
            ELSE 'RFDO' END AS SECTIONHEADING,
       CONCAT(page_namespace, '|', page_title) AS Page_NSTITLE_HISTLINK_TALKLINK,
       REPLACE(REPLACE(SUBSTRING_INDEX(cl_to, '_in_', -1), '_entries', ''), '_', ' ') AS Language_RAW,
       CONCAT('[https://www.ramselehof.de/wikipedia/wikiblame.php?lang=en&project=wiktionary&tld=org&article=',
        '{{{{subst:NS:', page_namespace, '}}}}:', page_title,
        '&force_wikitags=on&needle=%7B%7B',
        CASE WHEN cl_to LIKE 'Requests_for_verification_in_%' THEN 'rfv Find when RFV' ELSE 'rfd Find when RFD' END,
        ' tag was added]') AS Tools_RAW
FROM categorylinks
  INNER JOIN page ON cl_from = page_id
  LEFT JOIN (pagelinks INNER JOIN linktarget ON pl_target_id = lt_id) ON page_namespace = lt_namespace AND page_title = lt_title AND
    -- look for links from an RFV or RFD page
    pl_from IN (SELECT page_id
                FROM page
                WHERE page_namespace = 4 AND
                  (page_title LIKE 'Requests_for_verification/%' OR
                   page_title LIKE 'Requests_for_deletion/%') AND
                  page_title NOT LIKE '%rchive%')
WHERE
  -- return pages in an RFV or RFD category
  (cl_to LIKE 'Requests_for_verification_in_%' OR
   cl_to LIKE 'Requests_for_deletion_in_%' OR
   cl_to LIKE 'Requests_for_deletion/Others') AND
  -- only return pages which are not linked from an RFV or RFD page
  pl_from IS NULL
ORDER BY CASE WHEN cl_to LIKE 'Requests_for_verification_in_%'
              THEN 1
              WHEN cl_to LIKE 'Requests_for_deletion_in_%'
              THEN 2
              ELSE 3 END, Language_RAW, page_namespace, page_title
""",


"Recently recreated entries that failed RFV or RFD":
f"""
SELECT CONCAT(page_namespace, '|', page_title) AS Page_NSTITLE_HISTLINK_TALKLINK,
       {date('delete_log.log_timestamp')} AS Deletion_date_RAW,
       delete_comment.comment_text AS Deletion_reason_NOWIKI,
       {date('create_log.log_timestamp')} AS Recreation_date_RAW,
       CONCAT('{{{{user|', create_actor.actor_name, '}}}}') AS Recreator_RAW,
       create_comment.comment_text AS Recreation_edit_summary_NOWIKI
FROM page
  INNER JOIN logging_logindex AS delete_log ON page_namespace = delete_log.log_namespace AND page_title = delete_log.log_title
  INNER JOIN comment_logging AS delete_comment ON delete_log.log_comment_id = delete_comment.comment_id
  INNER JOIN logging_logindex AS create_log ON page_namespace = create_log.log_namespace AND page_title = create_log.log_title
  INNER JOIN comment_logging AS create_comment ON create_log.log_comment_id = create_comment.comment_id
  INNER JOIN actor_logging AS create_actor ON create_log.log_actor = create_actor.actor_id
WHERE
  -- look for main namespace entries only
  page_namespace = 0 AND
  -- restrict the log tables to appropriate log types
  delete_log.log_type = 'delete' AND
  create_log.log_type = 'create' AND
  -- look for deletion log entries that mention RFV or RFD
  (delete_comment.comment_text LIKE '%RFV%' OR delete_comment.comment_text LIKE '%rfv%' OR
   delete_comment.comment_text LIKE '%RFD%' OR delete_comment.comment_text LIKE '%rfd%') AND
  -- look for page recreations that occurred in the last 60 days
  create_log.log_timestamp > DATE_FORMAT(NOW() - INTERVAL 60 DAY, '%Y%m%d000000') AND
  -- the creation must be after the deletion
  create_log.log_timestamp > delete_log.log_timestamp
ORDER BY create_log.log_timestamp DESC
""",


"Derivation category does not match entry language":
f"""
SELECT REPLACE(CONCAT('In "', SUBSTRING_INDEX(derived_cat.cl_to, '_terms_', 1), ' terms derived from X" but not in "',
                      SUBSTRING_INDEX(derived_cat.cl_to, '_terms_', 1), ' lemmas" or "',
                      SUBSTRING_INDEX(derived_cat.cl_to, '_terms_', 1), ' non-lemma forms"'), '_', ' ') AS SECTIONHEADING,
       CONCAT(page_namespace, '|', page_title) AS Page_NSTITLE_EDITLINK
FROM page
  INNER JOIN categorylinks AS derived_cat ON page_id = derived_cat.cl_from
WHERE
  -- only keep main and Reconstruction namespace pages
  page_namespace IN (0, 118) AND
  -- look for "derived from" etymology categories
  derived_cat.cl_to LIKE '%_terms_derived_from_%' AND
  -- exclude language family categories
  derived_cat.cl_to NOT LIKE '%_languages' AND
  -- only return entries where the language name at the beginning of the "derived from" category does
  -- not correspond to any of the lemma/non-lemma forms categories the page is in
  NOT EXISTS (
    SELECT 1
    FROM categorylinks AS absent_lemma_nonlemma_cat
    WHERE
      absent_lemma_nonlemma_cat.cl_from = page_id AND
      (absent_lemma_nonlemma_cat.cl_to = CONCAT(SUBSTRING_INDEX(derived_cat.cl_to, '_terms_', 1), '_lemmas') OR
      absent_lemma_nonlemma_cat.cl_to = CONCAT(SUBSTRING_INDEX(derived_cat.cl_to, '_terms_', 1), '_non-lemma_forms') OR
      absent_lemma_nonlemma_cat.cl_to = CONCAT(SUBSTRING_INDEX(derived_cat.cl_to, '_terms_', 1), '_alternative_forms') OR
      absent_lemma_nonlemma_cat.cl_to = CONCAT(SUBSTRING_INDEX(derived_cat.cl_to, '_terms_', 1), '_kanji'))
  )
GROUP BY SECTIONHEADING, Page_NSTITLE_EDITLINK
ORDER BY SECTIONHEADING, {namespaceOrderBy('page_namespace')}, page_title
""",


# "Entries with no headword line":
# f"""
# SELECT CASE WHEN SUBSTRING(GROUP_CONCAT(REPLACE(present_cat.cl_to, '_', ' ')) FROM 1 FOR 1) BETWEEN 'A' AND 'Z'
#             THEN SUBSTRING(GROUP_CONCAT(REPLACE(present_cat.cl_to, '_', ' ')) FROM 1 FOR 1)
#             ELSE 'other' END AS SECTIONHEADING,
#        CONCAT(page_namespace, '|', page_title) AS Page_NSTITLE_EDITLINK,
#        GROUP_CONCAT(REPLACE(present_cat.cl_to, '_', ' ') SEPARATOR ', ') Categories_present_on_entry_RAW
# FROM page
#   LEFT JOIN categorylinks AS absent_lemma_nonlemma_cat ON page_id = absent_lemma_nonlemma_cat.cl_from AND
#      -- exclude pages in lemma and non-lemma categories
#     (cl_to LIKE '%_lemmas' OR
#      cl_to LIKE '%_non-lemma_forms' OR
#      -- exclude {{no entry}}
#      cl_to LIKE '%_entries_that_don_t_exist' OR
#      -- TODO temporarily exclude Russian entries, there are so many that they flood the results
#      -- cl_to LIKE 'Russian_%' OR
#      -- exclude Unicode character entries  TODO these are still problematic and should ultimately be cleaned up
#      cl_to LIKE '%_block' OR
#      -- exclude speedy deletion candidates
#      cl_to LIKE 'Candidates_for_speedy_deletion')
#   LEFT JOIN (templatelinks INNER JOIN linktarget ON tl_target_id = lt_id) ON page_id = tl_from AND
#      -- exclude pages transcluding Module:headword
#     ((lt_namespace = 828 AND lt_title = 'headword') OR
#      -- exclude pages transcluding certain CJK cross-reference templates
#      (lt_namespace = 10 AND lt_title IN ('ja-gv', 'ja-see', 'ja-see-kango', 'zh-see')))
#   LEFT JOIN categorylinks AS present_cat ON present_cat.cl_from = page_id
# WHERE
#   -- only keep main and Reconstruction namespace pages
#   page_namespace IN (0, 118) AND
#   -- skip redirects
#   NOT page_is_redirect AND
#   -- skip /translations and /derived_terms subpages
#   page_title NOT LIKE '%/translations' AND
#   page_title NOT LIKE '%/derived_terms' AND
#   -- only return pages which are not in one of the categories of interest
#   absent_lemma_nonlemma_cat.cl_to IS NULL AND
#   -- only return pages which do not transclude one of the templates/modules of interest
#   tl_target_id IS NULL
# GROUP BY Page_NSTITLE_EDITLINK, absent_lemma_nonlemma_cat.cl_to, tl_target_id
# ORDER BY SECTIONHEADING, Categories_present_on_entry_RAW, {namespaceOrderBy('page_namespace')}, page_title
# """,


# "Entries with missing headword lines": 
# f"""
# WITH q AS (
# SELECT CONCAT('[[:Category:', REPLACE(pages_with_n_entries_cat.cl_to, '_', ' '), ']]') AS SECTIONHEADING,
#   page_namespace,
#   page_title,
#   COUNT(DISTINCT REPLACE(REPLACE(REPLACE(lnl_cat.cl_to, '_lemmas', ''), '_non-lemma_forms', ''), "_entries_that_don't_exist", '')) AS lnl_count,
#   GROUP_CONCAT(REPLACE(lnl_cat.cl_to, '_', ' ') ORDER BY lnl_cat.cl_to SEPARATOR ', ') AS Category_names_RAW,
#   CAST(REPLACE(REPLACE(REPLACE(pages_with_n_entries_cat.cl_to, 'Pages_with_', ''), '_entry', ''), '_entries', '') AS integer) AS pwne_num
# FROM page
#   LEFT JOIN (templatelinks INNER JOIN linktarget ON tl_target_id = lt_id) ON page_id = tl_from AND ((lt_namespace = 10 AND
#     -- for the time being, completely exclude pages transcluding certain CJK cross-reference templates and {{cuns}}
#     lt_title IN ('ja-gv', 'ja-kanji', 'ja-see', 'ja-see-kango', 'zh-see', 'cuns')) OR (lt_namespace = 4 AND lt_title = 'Tracking/headword/unrecognized_pos'))
#   LEFT JOIN categorylinks AS chinese_lemmas_cat ON page_id = chinese_lemmas_cat.cl_from AND 
#     -- also completely exclude Chinese lemmas - the logic required to properly handle them makes the query too fragile
#     chinese_lemmas_cat.cl_to = 'Chinese_lemmas'
#   INNER JOIN categorylinks AS pages_with_n_entries_cat ON page_id = pages_with_n_entries_cat.cl_from
#   LEFT JOIN categorylinks AS lnl_cat ON page_id = lnl_cat.cl_from AND
#     -- look for lemma and non-lemma categories
#     (lnl_cat.cl_to LIKE '%_lemmas' OR
#      lnl_cat.cl_to LIKE '%_non-lemma_forms' OR
#      lnl_cat.cl_to LIKE '%_alternative_forms' OR
#      lnl_cat.cl_to LIKE '%_entries_that_don_t_exist') AND
#     -- ignore categories that look like lemma categories but actually are not
#     lnl_cat.cl_to NOT IN ('Old_Latin_lemmas', 'Old_Latin_non-lemma_forms') AND
#     lnl_cat.cl_to NOT LIKE '%_vowel_harmonic_lemmas'
# WHERE
#   -- main namespace only
#   page_namespace = 0 AND
#   -- only keep pages that do not have one of the excluded templates
#   tl_from IS NULL AND
#   -- only keep pages that are not in the excluded category 
#   chinese_lemmas_cat.cl_from IS NULL AND
#   -- look for a "Pages with N entr(y|ies)" category
#   pages_with_n_entries_cat.cl_to LIKE 'Pages_with_%_entr%'
# GROUP BY pages_with_n_entries_cat.cl_from
# HAVING pwne_num <> lnl_count
# )
# SELECT SECTIONHEADING,
#   CONCAT(page_namespace, '|', page_title) AS Page_NSTITLE_EDITLINK,
#   CONCAT(lnl_count, ' (', ABS(pwne_num - lnl_count), CASE WHEN lnl_count < pwne_num THEN ' missing)' ELSE ' additional)' END) AS Headword_cat_count_RAW,
#   Category_names_RAW
# FROM q
# ORDER BY pwne_num, lnl_count, page_namespace, page_title
# """,


"One-way links from entries to Thesaurus pages":
f"""
SELECT CONCAT(
        GROUP_CONCAT(SUBSTRING_INDEX(cl_to, '_thesaurus', 1) ORDER BY cl_to SEPARATOR ','), ' ',
        CASE WHEN SUBSTRING(thes_pg.page_title FROM 1 FOR 1) BETWEEN 'A' AND 'Z'
               OR SUBSTRING(thes_pg.page_title FROM 1 FOR 1) BETWEEN 'a' AND 'z'
             THEN CAST(UPPER(CONVERT(SUBSTRING(thes_pg.page_title FROM 1 FOR 1) USING utf8)) AS BINARY)
             ELSE 'other' END
       ) AS SECTIONHEADING,
       CONCAT(entry_pg.page_namespace, '|', entry_pg.page_title) AS Entry_NSTITLE_EDITLINK,
       CONCAT(thes_pg.page_namespace, '|', thes_pg.page_title) AS Links_to_Thesaurus_page_NSTITLE_EDITLINK
FROM page AS entry_pg
  INNER JOIN pagelinks AS entry_to_thes_pl ON entry_to_thes_pl.pl_from = entry_pg.page_id
  INNER JOIN linktarget AS entry_to_thes_lt ON entry_to_thes_pl.pl_target_id = entry_to_thes_lt.lt_id
  INNER JOIN page AS thes_pg ON thes_pg.page_namespace = entry_to_thes_lt.lt_namespace AND thes_pg.page_title = entry_to_thes_lt.lt_title
  INNER JOIN categorylinks AS thes_cat ON thes_cat.cl_from = thes_pg.page_id
  LEFT JOIN (pagelinks AS thes_to_entry_pl 
             INNER JOIN linktarget AS thes_to_entry_lt ON pl_target_id = lt_id) ON thes_to_entry_pl.pl_from = thes_pg.page_id AND
    thes_to_entry_lt.lt_namespace = entry_pg.page_namespace AND thes_to_entry_lt.lt_title = entry_pg.page_title
WHERE
  -- only keep main and Reconstruction namespace entries
  entry_pg.page_namespace IN (0, 118) AND
  -- thesaurus pages in the Thesaurus namespace
  thes_pg.page_namespace = 110 AND
  -- look for categories of the type "LANG thesaurus entries" to identify the likely language involved
  cl_to LIKE '%_thesaurus_entries' AND
  -- only return pages where there is no link from the Thesaurus page back to the entry
  thes_to_entry_pl.pl_from IS NULL
GROUP BY thes_pg.page_namespace, thes_pg.page_title, entry_pg.page_namespace, entry_pg.page_title, thes_to_entry_pl.pl_from
ORDER BY SECTIONHEADING, Links_to_Thesaurus_page_NSTITLE_EDITLINK, Entry_NSTITLE_EDITLINK
""",


}
